//globals $

// inputTag plugin part start 

(function ($) {
    "use strict";

    var defaultOptions = {
        tagClass: function (item) {
            return 'label label-info';
        },
        itemValue: function (item) {
            return item ? item.toString() : item;
        },
        itemText: function (item) {
            return this.itemValue(item);
        },
        freeInput: true,
        addOnBlur: true,
        maxTags: undefined,
        maxChars: undefined,
        confirmKeys: [13, 44],
        onTagExists: function (item, $tag) {
            $tag.hide().fadeIn();
        },
        trimValue: false,
        allowDuplicates: false
    };

    /**
     * Constructor function
     */
    function TagsInput(element, options) {
        this.itemsArray = [];

        this.$element = $(element);
        this.$element.hide();

        this.isSelect = (element.tagName === 'SELECT');
        this.multiple = (this.isSelect && element.hasAttribute('multiple'));
        this.objectItems = options && options.itemValue;
        this.placeholderText = element.hasAttribute('placeholder') ? this.$element.attr('placeholder') : '';
        this.inputSize = Math.max(1, this.placeholderText.length);

        this.$container = $('<div class="bootstrap-tagsinput"></div>');
        this.$input = $('<input type="text" placeholder="' + this.placeholderText + '"/>').appendTo(this.$container);

        this.$element.after(this.$container);

        var inputWidth = (this.inputSize < 3 ? 3 : this.inputSize) + "em";
        this.$input.get(0).style.cssText = "width: " + inputWidth + " !important;";
        this.build(options);
    }

    TagsInput.prototype = {
        constructor: TagsInput,

        /**
         * Adds the given item as a new tag. Pass true to dontPushVal to prevent
         * updating the elements val()
         */
        add: function (item, dontPushVal) {
            var self = this;

            if (self.options.maxTags && self.itemsArray.length >= self.options.maxTags)
                return;

            // Ignore falsey values, except false
            if (item !== false && !item)
                return;

            // Trim value
            if (typeof item === "string" && self.options.trimValue) {
                item = $.trim(item);
            }

            // Throw an error when trying to add an object while the itemValue option was not set
            if (typeof item === "object" && !self.objectItems)
                throw ("Can't add objects when itemValue option is not set");

            // Ignore strings only containg whitespace
            if (item.toString().match(/^\s*$/))
                return;

            // If SELECT but not multiple, remove current tag
            if (self.isSelect && !self.multiple && self.itemsArray.length > 0)
                self.remove(self.itemsArray[0]);

            if (typeof item === "string" && this.$element[0].tagName === 'INPUT') {
                var items = item.split(',');
                if (items.length > 1) {
                    for (var i = 0; i < items.length; i++) {
                        this.add(items[i], true);
                    }

                    if (!dontPushVal)
                        self.pushVal();
                    return;
                }
            }

            var itemValue = self.options.itemValue(item),
                itemText = self.options.itemText(item),
                tagClass = self.options.tagClass(item);

            // Ignore items allready added
            var existing = $.grep(self.itemsArray, function (item) {
                return self.options.itemValue(item) === itemValue;
            })[0];
            if (existing && !self.options.allowDuplicates) {
                // Invoke onTagExists
                if (self.options.onTagExists) {
                    var $existingTag = $(".tag", self.$container).filter(function () {
                        return $(this).data("item") === existing;
                    });
                    self.options.onTagExists(item, $existingTag);
                }
                return;
            }

            // if length greater than limit
            if (self.items().toString().length + item.length + 1 > self.options.maxInputLength)
                return;

            // raise beforeItemAdd arg
            var beforeItemAddEvent = $.Event('beforeItemAdd', {
                item: item,
                cancel: false
            });
            self.$element.trigger(beforeItemAddEvent);
            if (beforeItemAddEvent.cancel)
                return;

            // register item in internal array and map
            self.itemsArray.push(item);

            // add a tag element
            var $tag = $('<span class="tag ' + htmlEncode(tagClass) + '">' + htmlEncode(itemText) + '<span data-role="remove"></span></span>');
            $tag.data('item', item);
            self.findInputWrapper().before($tag);
            $tag.after(' ');

            // add <option /> if item represents a value not present in one of the <select />'s options
            if (self.isSelect && !$('option[value="' + encodeURIComponent(itemValue) + '"]', self.$element)[0]) {
                var $option = $('<option selected>' + htmlEncode(itemText) + '</option>');
                $option.data('item', item);
                $option.attr('value', itemValue);
                self.$element.append($option);
            }

            if (!dontPushVal)
                self.pushVal();

            // Add class when reached maxTags
            if (self.options.maxTags === self.itemsArray.length || self.items().toString().length === self.options.maxInputLength)
                self.$container.addClass('bootstrap-tagsinput-max');

            self.$element.trigger($.Event('itemAdded', {
                item: item
            }));
        },

        /**
         * Removes the given item. Pass true to dontPushVal to prevent updating the
         * elements val()
         */
        remove: function (item, dontPushVal) {
            var self = this;

            if (self.objectItems) {
                if (typeof item === "object")
                    item = $.grep(self.itemsArray, function (other) {
                        return self.options.itemValue(other) == self.options.itemValue(item);
                    });
                else
                    item = $.grep(self.itemsArray, function (other) {
                        return self.options.itemValue(other) == item;
                    });

                item = item[item.length - 1];
            }

            if (item) {
                var beforeItemRemoveEvent = $.Event('beforeItemRemove', {
                    item: item,
                    cancel: false
                });
                self.$element.trigger(beforeItemRemoveEvent);
                if (beforeItemRemoveEvent.cancel)
                    return;

                $('.tag', self.$container).filter(function () {
                    return $(this).data('item') === item;
                }).remove();
                $('option', self.$element).filter(function () {
                    return $(this).data('item') === item;
                }).remove();
                if ($.inArray(item, self.itemsArray) !== -1)
                    self.itemsArray.splice($.inArray(item, self.itemsArray), 1);
            }

            if (!dontPushVal)
                self.pushVal();

            // Remove class when reached maxTags
            if (self.options.maxTags > self.itemsArray.length)
                self.$container.removeClass('bootstrap-tagsinput-max');

            self.$element.trigger($.Event('itemRemoved', {
                item: item
            }));
        },

        /**
         * Removes all items
         */
        removeAll: function () {
            var self = this;

            $('.tag', self.$container).remove();
            $('option', self.$element).remove();

            while (self.itemsArray.length > 0)
                self.itemsArray.pop();

            self.pushVal();
        },

        /**
         * Refreshes the tags so they match the text/value of their corresponding
         * item.
         */
        refresh: function () {
            var self = this;
            $('.tag', self.$container).each(function () {
                var $tag = $(this),
                    item = $tag.data('item'),
                    itemValue = self.options.itemValue(item),
                    itemText = self.options.itemText(item),
                    tagClass = self.options.tagClass(item);

                // Update tag's class and inner text
                $tag.attr('class', null);
                $tag.addClass('tag ' + htmlEncode(tagClass));
                $tag.contents().filter(function () {
                    return this.nodeType == 3;
                })[0].nodeValue = htmlEncode(itemText);

                if (self.isSelect) {
                    var option = $('option', self.$element).filter(function () {
                        return $(this).data('item') === item;
                    });
                    option.attr('value', itemValue);
                }
            });
        },

        /**
         * Returns the items added as tags
         */
        items: function () {
            return this.itemsArray;
        },

        /**
         * Assembly value by retrieving the value of each item, and set it on the
         * element.
         */
        pushVal: function () {
            var self = this,
                val = $.map(self.items(), function (item) {
                    return self.options.itemValue(item).toString();
                });

            self.$element.val(val, true).trigger('change');
        },

        /**
         * Initializes the tags input behaviour on the element
         */
        build: function (options) {
            var self = this;

            self.options = $.extend({}, defaultOptions, options);
            // When itemValue is set, freeInput should always be false
            if (self.objectItems)
                self.options.freeInput = false;

            makeOptionItemFunction(self.options, 'itemValue');
            makeOptionItemFunction(self.options, 'itemText');
            makeOptionFunction(self.options, 'tagClass');

            // Typeahead Bootstrap version 2.3.2
            if (self.options.typeahead) {
                var typeahead = self.options.typeahead || {};

                makeOptionFunction(typeahead, 'source');

                self.$input.typeahead($.extend({}, typeahead, {
                    source: function (query, process) {
                        function processItems(items) {
                            var texts = [];

                            for (var i = 0; i < items.length; i++) {
                                var text = self.options.itemText(items[i]);
                                map[text] = items[i];
                                texts.push(text);
                            }
                            process(texts);
                        }

                        this.map = {};
                        var map = this.map,
                            data = typeahead.source(query);

                        if ($.isFunction(data.success)) {
                            // support for Angular callbacks
                            data.success(processItems);
                        } else if ($.isFunction(data.then)) {
                            // support for Angular promises
                            data.then(processItems);
                        } else {
                            // support for functions and jquery promises
                            $.when(data)
                                .then(processItems);
                        }
                    },
                    updater: function (text) {
                        self.add(this.map[text]);
                    },
                    matcher: function (text) {
                        return (text.toLowerCase().indexOf(this.query.trim().toLowerCase()) !== -1);
                    },
                    sorter: function (texts) {
                        return texts.sort();
                    },
                    highlighter: function (text) {
                        var regex = new RegExp('(' + this.query + ')', 'gi');
                        return text.replace(regex, "<strong>$1</strong>");
                    }
                }));
            }

            // typeahead.js
            if (self.options.typeaheadjs) {
                var typeaheadjs = self.options.typeaheadjs || {};

                self.$input.typeahead(null, typeaheadjs).on('typeahead:selected', $.proxy(function (obj, datum) {
                    if (typeaheadjs.valueKey)
                        self.add(datum[typeaheadjs.valueKey]);
                    else
                        self.add(datum);
                    self.$input.typeahead('val', '');
                }, self));
            }

            self.$container.on('click', $.proxy(function (event) {
                if (!self.$element.attr('disabled')) {
                    self.$input.removeAttr('disabled');
                }
                self.$input.focus();
            }, self));

            if (self.options.addOnBlur && self.options.freeInput) {
                self.$input.on('focusout', $.proxy(function (event) {
                    // HACK: only process on focusout when no typeahead opened, to
                    //       avoid adding the typeahead text as tag
                    if ($('.typeahead, .twitter-typeahead', self.$container).length === 0) {
                        self.add(self.$input.val());
                        self.$input.val('');
                    }
                }, self));
            }


            self.$container.on('keydown', 'input', $.proxy(function (event) {
                var $input = $(event.target),
                    $inputWrapper = self.findInputWrapper();

                if (self.$element.attr('disabled')) {
                    self.$input.attr('disabled', 'disabled');
                    return;
                }

                switch (event.which) {
                    // BACKSPACE
                case 8:
                    if (doGetCaretPosition($input[0]) === 0) {
                        var prev = $inputWrapper.prev();
                        if (prev) {
                            self.remove(prev.data('item'));
                        }
                    }
                    break;

                    // DELETE
                case 46:
                    if (doGetCaretPosition($input[0]) === 0) {
                        var next = $inputWrapper.next();
                        if (next) {
                            self.remove(next.data('item'));
                        }
                    }
                    break;

                    // LEFT ARROW
                case 37:
                    // Try to move the input before the previous tag
                    var $prevTag = $inputWrapper.prev();
                    if ($input.val().length === 0 && $prevTag[0]) {
                        $prevTag.before($inputWrapper);
                        $input.focus();
                    }
                    break;
                    // RIGHT ARROW
                case 39:
                    // Try to move the input after the next tag
                    var $nextTag = $inputWrapper.next();
                    if ($input.val().length === 0 && $nextTag[0]) {
                        $nextTag.after($inputWrapper);
                        $input.focus();
                    }
                    break;
                default:
                    // ignore
                }

                // Reset internal input's size
                var textLength = $input.val().length,
                    wordSpace = Math.ceil(textLength / 5),
                    size = textLength + wordSpace + 1;
                $input.attr('size', Math.max(this.inputSize, $input.val().length));
            }, self));

            self.$container.on('keypress', 'input', $.proxy(function (event) {
                var $input = $(event.target);

                if (self.$element.attr('disabled')) {
                    self.$input.attr('disabled', 'disabled');
                    return;
                }

                var text = $input.val(),
                    maxLengthReached = self.options.maxChars && text.length >= self.options.maxChars;
                if (self.options.freeInput && (keyCombinationInList(event, self.options.confirmKeys) || maxLengthReached)) {
                    self.add(maxLengthReached ? text.substr(0, self.options.maxChars) : text);
                    $input.val('');
                    event.preventDefault();
                }

                // Reset internal input's size
                var textLength = $input.val().length,
                    wordSpace = Math.ceil(textLength / 5),
                    size = textLength + wordSpace + 1;
                $input.attr('size', Math.max(this.inputSize, $input.val().length));
            }, self));

            // Remove icon clicked
            self.$container.on('click', '[data-role=remove]', $.proxy(function (event) {
                if (self.$element.attr('disabled')) {
                    return;
                }
                self.remove($(event.target).closest('.tag').data('item'));
            }, self));

            // Only add existing value as tags when using strings as tags
            if (self.options.itemValue === defaultOptions.itemValue) {
                if (self.$element[0].tagName === 'INPUT') {
                    self.add(self.$element.val());
                } else {
                    $('option', self.$element).each(function () {
                        self.add($(this).attr('value'), true);
                    });
                }
            }
        },

        /**
         * Removes all tagsinput behaviour and unregsiter all event handlers
         */
        destroy: function () {
            var self = this;

            // Unbind events
            self.$container.off('keypress', 'input');
            self.$container.off('click', '[role=remove]');

            self.$container.remove();
            self.$element.removeData('tagsinput');
            self.$element.show();
        },

        /**
         * Sets focus on the tagsinput
         */
        focus: function () {
            this.$input.focus();
        },

        /**
         * Returns the internal input element
         */
        input: function () {
            return this.$input;
        },

        /**
         * Returns the element which is wrapped around the internal input. This
         * is normally the $container, but typeahead.js moves the $input element.
         */
        findInputWrapper: function () {
            var elt = this.$input[0],
                container = this.$container[0];
            while (elt && elt.parentNode !== container)
                elt = elt.parentNode;

            return $(elt);
        }
    };

    /**
     * Register JQuery plugin
     */
    $.fn.tagsinput = function (arg1, arg2) {
        var results = [];

        this.each(function () {
            var tagsinput = $(this).data('tagsinput');
            // Initialize a new tags input
            if (!tagsinput) {
                tagsinput = new TagsInput(this, arg1);
                $(this).data('tagsinput', tagsinput);
                results.push(tagsinput);

                if (this.tagName === 'SELECT') {
                    $('option', $(this)).attr('selected', 'selected');
                }

                // Init tags from $(this).val()
                $(this).val($(this).val());
            } else if (!arg1 && !arg2) {
                // tagsinput already exists
                // no function, trying to init
                results.push(tagsinput);
            } else if (tagsinput[arg1] !== undefined) {
                // Invoke function on existing tags input
                var retVal = tagsinput[arg1](arg2);
                if (retVal !== undefined)
                    results.push(retVal);
            }
        });

        if (typeof arg1 == 'string') {
            // Return the results from the invoked function calls
            return results.length > 1 ? results : results[0];
        } else {
            return results;
        }
    };

    $.fn.tagsinput.Constructor = TagsInput;

    /**
     * Most options support both a string or number as well as a function as
     * option value. This function makes sure that the option with the given
     * key in the given options is wrapped in a function
     */
    function makeOptionItemFunction(options, key) {
        if (typeof options[key] !== 'function') {
            var propertyName = options[key];
            options[key] = function (item) {
                return item[propertyName];
            };
        }
    }

    function makeOptionFunction(options, key) {
        if (typeof options[key] !== 'function') {
            var value = options[key];
            options[key] = function () {
                return value;
            };
        }
    }
    /**
     * HtmlEncodes the given value
     */
    var htmlEncodeContainer = $('<div />');

    function htmlEncode(value) {
        if (value) {
            return htmlEncodeContainer.text(value).html();
        } else {
            return '';
        }
    }

    /**
     * Returns the position of the caret in the given input field
     * http://flightschool.acylt.com/devnotes/caret-position-woes/
     */
    function doGetCaretPosition(oField) {
        var iCaretPos = 0;
        if (document.selection) {
            oField.focus();
            var oSel = document.selection.createRange();
            oSel.moveStart('character', -oField.value.length);
            iCaretPos = oSel.text.length;
        } else if (oField.selectionStart || oField.selectionStart == '0') {
            iCaretPos = oField.selectionStart;
        }
        return (iCaretPos);
    }

    /**
     * Returns boolean indicates whether user has pressed an expected key combination.
     * @param object keyPressEvent: JavaScript event object, refer
     *     http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
     * @param object lookupList: expected key combinations, as in:
     *     [13, {which: 188, shiftKey: true}]
     */
    function keyCombinationInList(keyPressEvent, lookupList) {
        var found = false;
        $.each(lookupList, function (index, keyCombination) {
            if (typeof (keyCombination) === 'number' && keyPressEvent.which === keyCombination) {
                found = true;
                return false;
            }

            if (keyPressEvent.which === keyCombination.which) {
                var alt = !keyCombination.hasOwnProperty('altKey') || keyPressEvent.altKey === keyCombination.altKey,
                    shift = !keyCombination.hasOwnProperty('shiftKey') || keyPressEvent.shiftKey === keyCombination.shiftKey,
                    ctrl = !keyCombination.hasOwnProperty('ctrlKey') || keyPressEvent.ctrlKey === keyCombination.ctrlKey;
                if (alt && shift && ctrl) {
                    found = true;
                    return false;
                }
            }
        });

        return found;
    }

    /**
     * Initialize tagsinput behaviour on inputs and selects which have
     * data-role=tagsinput
     */
    $(function () {
        $("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
    });
})(window.jQuery);

// inputTag plugin part end 

// HTML5 Sortable jQuery Plugin part start 

(function ($) {
    var dragging, placeholders = $();
    $.fn.sortable = function (options) {
        var method = String(options);
        options = $.extend({
            connectWith: false
        }, options);
        return this.each(function () {
            if (/^enable|disable|destroy$/.test(method)) {
                var items = $(this).children($(this).data('items')).attr('draggable', method == 'enable');
                if (method == 'destroy') {
                    items.add(this).removeData('connectWith items')
                        .off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
                }
                return;
            }
            var isHandle, index, items = $(this).children(options.items);
            var placeholder = $('<' + (/^ul|ol$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');
            items.find(options.handle).mousedown(function () {
                isHandle = true;
            }).mouseup(function () {
                isHandle = false;
            });
            $(this).data('items', options.items)
            placeholders = placeholders.add(placeholder);
            if (options.connectWith) {
                $(options.connectWith).add(this).data('connectWith', options.connectWith);
            }
            items.attr('draggable', 'true').on('dragstart.h5s', function (e) {
                if (options.handle && !isHandle) {
                    return false;
                }
                isHandle = false;
                var dt = e.originalEvent.dataTransfer;
                dt.effectAllowed = 'move';
                dt.setData('Text', 'dummy');
                index = (dragging = $(this)).addClass('sortable-dragging').index();
            }).on('dragend.h5s', function () {
                dragging.removeClass('sortable-dragging').show();
                placeholders.detach();
                if (index != dragging.index()) {
                    items.parent().trigger('sortupdate', {
                        item: dragging
                    });
                }
                dragging = null;
            }).not('a[href], img').on('selectstart.h5s', function () {
                this.dragDrop && this.dragDrop();
                return false;
            }).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function (e) {
                if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
                    return true;
                }
                if (e.type == 'drop') {
                    e.stopPropagation();
                    placeholders.filter(':visible').after(dragging);
                    return false;
                }
                e.preventDefault();
                e.originalEvent.dataTransfer.dropEffect = 'move';
                if (items.is(this)) {
                    if (options.forcePlaceholderSize) {
                        placeholder.height(dragging.outerHeight());
                    }
                    dragging.hide();
                    $(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
                    placeholders.not(placeholder).detach();
                } else if (!placeholders.is(this) && !$(this).children(options.items).length) {
                    placeholders.detach();
                    $(this).append(placeholder);
                }
                return false;
            });
        });
    };
})(jQuery);

// HTML5 Sortable jQuery Plugin part end 

// jQuery Cookie plugin start

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (value !== undefined && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires,
                    t = options.expires = new Date();
                t.setTime(+t + days * 864e+5);
            }

            return (document.cookie = [
    encode(key), '=', stringifyCookieValue(value),
    options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
    options.path ? '; path=' + options.path : '',
    options.domain ? '; domain=' + options.domain : '',
    options.secure ? '; secure' : ''
   ].join(''));
        }

        // Read

        var result = key ? undefined : {};

        // To prevent the for loop in the first place assign an empty array
        // in case there are no cookies at all. Also prevents odd result when
        // calling $.cookie().
        var cookies = document.cookie ? document.cookie.split('; ') : [];

        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = parts.join('=');

            if (key && key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }

        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, {
            expires: -1
        }));
        return !$.cookie(key);
    };

}));

// jQuery Cookie plugin stop

// initGuid part start 

function initGuid() {
    "use strict";
    var mainData,
        curListLv1,
        curListLv2,
        curLevel = 0,
        API = {
            indexAll: "api/policy/indexAll",
            delPolicy: "api/policy/delete/",
            addFirstLevel: "api/policy/addFirstLevel",
            addSecondLevel: "api/policy/addSecondLevel",
            addThirdLevel: "api/policy/addThirdLevel",
            getPolicy: "api/policy/get/",
            getPolicyContent: "api/policy/getContent/",
            updatePolicy: "api/policy/update/"
        };
    $("#sumNote .sum-canvas").summernote();

    $("#fixPanel .btnClose").click(function (e) {
        closeFixPanel();
    });

    function closeFixPanel() {
        $("#fixPanel").fadeOut(200, function () {
            $("#form-guide4")[0].reset();
            $("input.tagsinput").tagsinput('removeAll');
            $("#sumNote .sum-canvas").code("");
            $("body").css("overflow", "auto");
        });
    }

    function gE(tag, cls, html) {
        return $("<" + tag + ">", {
            'class': cls
        }).html(html);
    }

    function collForm1() {
        $("#step1").fadeOut(0);
        $("#form-guide")[0].reset();
    }

    function collForm2_1() {
        $("#form-guide2")[0].reset();
    }

    function collForm2_2() {
        $("#form-guide3")[0].reset();
    }

    function collForm2_3() {
        closeFixPanel();
        $("#form-guide4")[0].reset();
        $("input.tagsinput").tagsinput('removeAll');
    }

    function collForm6_2() {
        $("#form-guide62")[0].reset();
    }

    function openFirstLevel(l, $t, ele) {
        collForm1();
        l.children(".selected").removeClass("selected");
        $("#step6 div.content").empty();
        $("#step6").hide(0);
        $t.addClass("selected");
        curListLv1 = ele.id;
        $("#step2").fadeIn();
        if ($t.data("type") === "list") {
            $("#step2 div.urlcontent,#step2 div.content").empty();
            $("#btn-addLv2").fadeIn(0);

            var al = [];
            $.each(mainData[$t.data("idx")].children, function (index, item) {
                var url = item.id;
                if (item.type === "url") {
                    url = item.url;
                }
                al.push(gE("p", "item-lv2 " + item.type, item.title + " - " + item.type + " - " + url).data({
                    "id": item.id,
                    "type": item.type
                }).bind("click", openThirdLevel));
            });
            $("#step2 div.content").html(al);
        } else if ($t.data("type") === "url") {
            $("#step2 div.urlcontent,#step2 div.content").empty();
            $("#btn-addLv2").show(0);
            var _child = mainData[$t.data("idx")].children;
            if (_child != null) {
                if (_child.length > 0) {
                    var al = [];
                    $.each(mainData[$t.data("idx")].children, function (index, item) {
                        var url = item.id;
                        if (item.type === "url") {
                            url = item.url;
                        }
                        al.push(gE("p", "item-lv2 " + item.type, item.title + " - " + item.type + " - " + url).data({
                            "id": item.id,
                            "type": item.type
                        }).bind("click", openThirdLevel));
                    });
                    $("#step2 div.content").append(al);


                }
            }

            var _a = mainData[$t.data("idx")].url;
            var t = gE("a").attr("href", _a).append(gE("p", "url-v2", _a));
            $("#step2 div.urlcontent").append(gE("span", "", "交互页面地址：")).append(t);
        } else {
            $("#btn-addLv2").fadeOut(0);
            $("#step2 div.content").html("");
            alert("一级列表对象有误");
        }
    }

    function getLevel1(l) {
        //url: "lv.json",
        //url: "indexAll"
        $.ajax({
            url: API.indexAll,
            processData: false,
            contentType: false,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                console.log(data);
                mainData = data.body;
                //                l.removeClass("loading");
                $.each(mainData, function (idx, ele) {
                    var _e = gE("li", ele.type, ele.title);
                    _e.data({
                        "type": ele.type,
                        "idx": idx,
                        "id": ele.id
                    });
                    _e.click(function (e) {
                        openFirstLevel(l, $(this), ele);
                    });
                    l.append(_e);
                });
            },
            error: function (dt) {
                console.log(dt);
            }
        });

    }

    function openThirdLevel(e) {
        var $ct = $(e.currentTarget);
        var dt = $ct.data();
        $ct.addClass("selected").siblings().removeClass("selected");
        console.log(dt);
        if (dt.type == "list") {
            curListLv2 = dt.id;

            $("#step6 div.urlcontent,#step6 div.content").empty();
            var al = [];
            $("#btn-addLv3").show(0);
            $("#addLevel2").hide(0);
            console.log(mainData[$("#list li.selected").data("idx")].children);
            $.each(mainData[$("#list li.selected").data("idx")].children[$("#step2 .content p.selected").index()].children, function (index, item) {
                var url = item.id;
                if (item.type === "url") {
                    url = item.url;
                }
                al.push(gE("p", "item-lv2 " + item.type, item.title + " - " + item.type + " - " + url).data({
                    "id": item.id,
                    "type": item.type
                }).click(function (e) {
                    console.log(item);
                    $(this).addClass("selected").siblings().removeClass("selected");
                }));
            });
            $("#step6 div.content").html(al);
            $("#step6").show(0);
        } else {
            $("#step6 div.content").empty();
            $("#step6").hide(0);
        }
    }

    function delList(tarId, isItem, callback) {
        $.ajax({
            url: API.delPolicy + tarId,
            type: 'GET',
            data: {
                isItem: isItem
            },
            dataType: 'json',
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                console.log(data);
                if (arguments.length > 2) {
                    callback();
                }
            },
            error: function (dt) {
                console.log(dt);
            }
        });
    }

    function getContent(li) {
        $("#fixPanel").fadeIn(200);
        $("body").css("overflow", "hidden");
        $.ajax({
            url: API.getPolicy + li.data("id") + "?isItem=true",
            type: 'GET',
            async: false,
            dataType: 'json',
            success: function (dt) {
                if (dt.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (dt.status === "0") {
                    var here = $("#fixPanel");
                    here.find("input[name=title]").val(dt.body.title);
                    here.find("input[name=description]").val(dt.body.description);
                    here.find("input[name=keywords]").val(dt.body.keywords);
                    here.find("input[name=keywords]").tagsinput("add", dt.body.keywords);
                }
            }
        });
        $.ajax({
            url: API.getPolicyContent + li.data("id") + "?isItem=true",
            type: 'GET',
            dataType: 'text',
            success: function (dt) {
                if (dt.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (dt === "") {
                    alert("获取失败..");
                } else {
                    $("#sumNote .sum-canvas").code(dt);
                }
            }
        });

    }

    function initGuidLv1() {

        getLevel1($("#list"));
        var isEdit = false;
        var opt = [
            {},
            {
                form: "form-guide4",
                title: "#form-guide4 input[name=title]",
                type: "#addLevel2 input[name=sub2]:checked",
                content: "#step2 div.content"
  },
            {
                form: "form-guide4",
                title: "#form-guide4 input[name=title]",
                type: "#addLevel3 input[name=sub3]:checked",
                content: "#step6 div.content"
  }
 ];

        $("#btnDelete").click(function (e) {
            if (!confirm('确认删除?')) {
                return false;
            }

            var li = $("#list li.selected");
            if (li.length > 0) {
                delList(li.data("id"), false, function () {
                    mainData.splice(li.data("idx"), 1);
                    li.remove();
                    $("#list li").each(function(index,item){
                        $(item).data("idx",index);
                    });
                    console.log("deleted");
                });
            } else {
                alert("请选择分类");
            }
        });
        $("#btn-delLv2").click(function (e) {
            if (!confirm('确认删除?')) {
                return false;
            }
            var li = $("#step2 .content p.selected");
            if (li.length > 0) {
                delList(li.data("id"), li.data("type") == "list" ? false : true, function () {
                    mainData[$("#list li.selected").index()].children.splice(li.index(), 1);
                    li.remove();
                    console.log("Lv2 Deleted");
                });
            } else {
                alert("请选择二级列表项");
            }
        });
        $("#btn-delLv3").click(function (e) {
            if (!confirm('确认删除?')) {
                return false;
            }
            var li = $("#step6 .content p.selected");
            if (li.length > 0) {
                console.log(li.data("type"));

                delList(li.data("id"), true, function () {
                    mainData[$("#list li.selected").index()].children[$("#step2 .content p.selected").index()].children.splice(li.index(), 1);
                    li.remove();
                    console.log("Lv3 Deleted");
                });
            } else {
                alert("请选择三级列表项");
            }
        });

        $("#btn-Editor").click(function (e) {
            console.log("isEdit: " + isEdit);
            var form;
            var op = opt[curLevel];
            var _t = $(op.title).val();
            var _tp = $(op.type).val();
            var _pid = "";
            var li;
            if (curLevel == 1) {
                _pid = curListLv1;
                li = $("#step2 .content p.selected");
            } else if (curLevel == 2) {
                _pid = curListLv2;
                li = $("#step6 .content p.selected");
            } else {
                alert("curLevel error!");
            }
            $("#" + op.form).find("input[name=pid]").val(_pid);
            $("#" + op.form).find("input[name=content]").val($("#sumNote .sum-canvas").code());
            form = $("#" + op.form).serialize();
            if (isEdit === false) {
                //add
                $.ajax({
                    url: API.addThirdLevel,
                    data: form,
                    type: 'POST',
                    dataType: 'json',
                    success: function (dt) {
                        if (dt.status === "3") {
                            alert(LANG.msg.error[3]);
                            location.href = "login.html";
                        }
                        console.log(dt);
                        if (dt.status.toString() == "0") {
                            if (curLevel == 1) {
                                $(op.content).append(gE("p", "item-lv2", _t + " - " + _tp + " - " + dt.body.id).data({
                                    "id": dt.body.id,
                                    "type": _tp
                                }).bind("click", openThirdLevel));

                                mainData[$("#list").find("li.selected").data("idx")].children.push({
                                    id: dt.body.id,
                                    type: _tp,
                                    title: _t,
                                    children: []
                                });
                                alert("创建LV2-web成功");
                            } else if (curLevel == 2) {
                                mainData[$("#list").find("li.selected").data("idx")].children[$("#step2 div.content p.selected").index()].children.push({
                                    id: dt.body.id,
                                    type: _tp,
                                    title: _t,
                                    children: []
                                });
                                $(op.content).append(gE("p", "item-lv2", _t + " - " + _tp + " - " + dt.body.id).data({
                                    "id": dt.body.id,
                                    "type": _tp
                                }).click(function (e) {
                                    $(this).addClass("selected").siblings().removeClass("selected");
                                }));
                                alert("创建LV3-web成功");
                            } else {
                                alert("curLevel error!");
                            }
                            collForm2_3();
                        }
                    },
                    error: function (dt) {
                        console.log(dt);
                    }
                });
            } else {
                //edit
                $.ajax({
                    url: API.updatePolicy + li.data("id"),
                    data: form + "&isItem=true",
                    type: 'POST',
                    dataType: 'json',
                    success: function (dt) {
                        if (dt.status === "3") {
                            alert(LANG.msg.error[3]);
                            location.href = "login.html";
                        }
                        console.log(dt);
                        if (dt.status !== "0") {
                            alert("错误..");
                        } else {
                            if (curLevel == 1) {
                            mainData[$("#list li.selected").index()].children.splice(li.index(), 1);
                                mainData[$("#list").find("li.selected").data("idx")].children.push({
                                    id: dt.body,
                                    type: "web",
                                    title: $(op.title).val(),
                                    children: []
                                });
                                /*
                                li.html($(op.title).val() + " - " + li.data("type") + " - " + li.data("id"));
*/
                                $("#list").find("li.selected").click();
                                alert("修改LV2-web成功");
                            } else if (curLevel == 2) {
                                mainData[$("#list li.selected").index()].children[$("#step2 .content p.selected").index()].children.splice(li.index(), 1);
                                mainData[$("#list").find("li.selected").data("idx")].children[$("#step2 div.content p.selected").index()].children.push({
                                    id: dt.body,
                                    type: "web",
                                    title: $(op.title).val(),
                                    children: []
                                });
/*
                                li.html($(op.title).val() + " - " + li.data("type") + " - " + li.data("id"));
*/ 
                                $("#step2 div.content p.selected").click();                                
                                alert("修改LV3-web成功");
                            } else {
                                alert("curLevel error!");
                            }
                            collForm2_3();
                        }
                    },
                    error: function (dt) {
                        console.log(dt);
                    }
                });
            }
        });
        //$("#addLevel2 input:radio[value="+$(this).data("type")+"]").prop("checked",true);
        $("#addLevel2 input:radio").click(function (e) {
            /*
			list: 类型为包含子列表 可配置自列表
			web: 类型为文章 不需配置子列表
			url: 类型为url链接 不需配置子列表
		*/

            curLevel = 1;

            switch ($(this).val()) {
            case "list":
                $("#step5").hide(0);
                $("#step3").show(0);
                break;
            case "web":
                $("#fixPanel").fadeIn(200);
                $("body").css("overflow", "hidden");
                $("#step3,#step5").hide(0);
                break;
            case "url":
                $("#step5").show(0);
                $("#step3").hide(0);
                break;
            default:
                alert("addLevel2 error");
            }
        });
        $("#btn-confirmGd2").click(function (e) {
            var form;
            form = new FormData(document.getElementById("form-guide2"));
            form.append("pid", curListLv1);
            var _t = $("#form-guide2 input[name=title]").val();
            var _tp = $("#addLevel2 input[name=sub2]:checked").val();
            console.log(_t);
            $.ajax({
                url: API.addSecondLevel,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (dt) {
                    if (dt.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    console.log(dt);
                    if (dt.status.toString() == "0") {
                        mainData[$("#list").find("li.selected").data("idx")].children.push({
                            id: dt.body.id,
                            type: _tp,
                            title: _t,
                            children: []
                        });
                        $("#step2 div.content").append(gE("p", "item-lv2 list", _t + " - " + _tp + " - " + dt.body.id).data({
                            "id": dt.body.id,
                            "type": _tp
                        }).bind("click", openThirdLevel));
                        collForm2_1();
                        alert("创建LV2-List成功");
                    }
                },
                error: function (dt) {
                    console.log(dt);
                }
            });
        });
        $("#addLevel3 input:radio").click(function (e) {
            /*
			web: 类型为文章 不需配置子列表
			url: 类型为url链接 不需配置子列表
		*/
            curLevel = 2;
            isEdit = false;
            switch ($(this).val()) {
            case "web":
                $("#fixPanel").fadeIn(200);
                $("body").css("overflow", "hidden");
                $("#step6-2").hide(0);
                break;
            case "url":
                $("#step6-2").show(0);
                break;
            default:
                alert("addLevel3 error");
            }
        });
        $("#btn-confirmGd3").click(function (e) {
            var form;
            form = new FormData(document.getElementById("form-guide3"));
            form.append("pid", curListLv1);
            var _t = $("#form-guide3 input[name=title]").val();
            var _tp = $("#addLevel2 input[name=sub2]:checked").val();
            var _con = $("#form-guide3 input[name=url]").val();
            form.append("content", "");
            form.append("description", "");
            form.append("keywords", "");
            $.ajax({
                url: API.addThirdLevel,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (dt) {
                    if (dt.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    console.log(dt);
                    if (dt.status.toString() == "0") {
                        mainData[$("#list").find("li.selected").data("idx")].children.push({
                            id: dt.body.id,
                            type: _tp,
                            title: _t,
                            url: _con,
                            children: []
                        });
                        $("#step2 div.content").append(gE("p", "item-lv2", _t + " - " + _tp + " - " + _con).data({
                            "id": dt.body.id,
                            "type": _tp
                        }).bind("click", openThirdLevel));
                        collForm2_2();
                        alert("创建LV2-URL成功");
                    }
                },
                error: function (dt) {
                    console.log(dt);
                }
            });
        });
        $("#btn-confirmGd62").click(function (e) {
            var form;
            form = new FormData(document.getElementById("form-guide62"));
            form.append("pid", curListLv2);
            form.append("content", "");
            form.append("description", "");
            form.append("keywords", "");
            var _t = $("#form-guide62 input[name=title]").val();
            var _tp = $("#addLevel3 input[name=sub3]:checked").val();
            var _con = $("#form-guide62 input[name=url]").val();
            $.ajax({
                url: API.addThirdLevel,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (dt) {
                    if (dt.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    console.log(dt);
                    if (dt.status.toString() == "0") {
                        mainData[$("#list").find("li.selected").data("idx")].children[$("#step2 div.content p.selected").index()].children.push({
                            id: dt.body.id,
                            type: _tp,
                            title: _t,
                            url: _con,
                            children: []
                        });
                        $("#step6 div.content").append(gE("p", "item-lv2 " + _tp, _t + " - " + _tp + " - " + _con).data({
                            "id": dt.body.id,
                            "type": _tp
                        }).click(function (e) {
                            $(this).addClass("selected").siblings().removeClass("selected");
                        }));
                        collForm6_2();
                        alert("创建LV3-URL成功");
                    }
                },
                error: function (dt) {
                    console.log(dt);
                }
            });
        });
        $("#btn-addLv3").click(function (e) {
            curLevel = 2;
            isEdit = false;
            $("#addLevel3").fadeIn();
        });
        $("#btn-addLv2").click(function (e) {
            curLevel = 1;
            isEdit = false;
            $("#addLevel2").fadeIn();
            $("#step6").hide(0);
        });

        $("#btnAdd").click(function (e) {
            if ($("#list").children().length == 0) {
                //return false;
            }
            $("#step1").fadeIn();
            $("#step2").hide(0);
            $("#step6").hide(0);
        });
        $("#btn-cancel").click(function (e) {
            collForm1();
        });
        $("#jump").click(function (e) {
            if ($(this).prop("checked")) {
                $("#urlblock").show();
            } else {
                $("#urlblock").hide(0, function () {
                    $(this).children("input").val("")
                });
            }
        });
        $("#btn-submit").click(function () {
            var form;
            form = new FormData(document.getElementById("form-guide"));
            console.log("submit");
            $.ajax({
                url: API.addFirstLevel,
                data: form,
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (dt) {
                    if (dt.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    console.log(dt);
                    if (dt.status.toString() == "0") {
                        var _l = $("#list");
                        var _tp = $("#jump").val() === true ? "url" : "list";
                        var _t = $("#form-guide input[name=title]").val();
                        var _e = gE("li", _tp, _t);
                        _e.data({
                            "type": _tp,
                            "idx": _l.find("li").length,
                            "id": dt.body.id
                        });
                        mainData.push({
                            id: dt.body.id,
                            type: _tp,
                            title: _t,
                            children: []
                        });
                        _e.click(function (e) {
                            var ele = {
                                id: dt.body.id
                            };
                            openFirstLevel(_l, $(this), ele);
                        });
                        _l.append(_e);
                        console.log(_e.data());
                        collForm1();

                        alert("创建成功");
                    }
                },
                error: function (dt) {
                    console.log(dt);
                }
            });
        });

        $("#btn-editLv2").click(function (e) {
            isEdit = true;
            curLevel = 1;
//            $("#sumNote .sum-canvas").destroy();
            var li = $("#step2 .content p.selected");
            if (li.length > 0) {
                if (li.data("type") === "web") {
                    getContent(li);
                    console.log("Lv2 Editted");
                } else {
                    alert("请选择WEB项");
                }
            } else {
                alert("请选择二级列表项");
            }
        });
        $("#btn-editLv3").click(function (e) {
            isEdit = true;
            curLevel = 2;
            var li = $("#step6 .content p.selected");
            if (li.length > 0) {
                if (li.data("type") === "web") {
                    getContent(li);
                    console.log("Lv3 Editted");
                } else {
                    alert("请选择WEB项");
                }
            } else {
                alert("请选择三级列表项");
            }
        });
    }
    initGuidLv1();
}

// initGuid part start