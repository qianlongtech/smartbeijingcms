/*globals $, alert, console, FormData, confirm */
/*jslint regexp: true*/

//serv_url_assets后接路径查看图片
var serv_url_assets = "http://mmsimg.qianlong.com", //formal
//var serv_url_assets = "http://223.202.51.201", //develop
    POSITION,
    PAGE,
    TITLE,
    CHANNEL,
    ID,
    ADVTYPE,
    API = {
        //前缀为 http://mmscms.qianlong.com/SmartBeijingCMS/
        
        //Image
        viewImage: "api/images/showImage/",
        uploadScale: "upload/scale",    //formal
        uploadImage: "upload/news/image",   //formal
//        uploadScale: "api/upload/scale",    //develop
//        uploadImage: "api/upload/news/image",   //develop
        saveImage: "api/news/saveImage",
        
        //News
        addNews: [
            "api/news/pub",
            "api/democracy/pub"
        ],
        getNews: [
            "api/news/news/",
            "api/democracy/democ_mobile/"
        ],
        getNewsContent: [
            "api/news/news/html/",
            "api/democracy/news/html/"
        ],
        getNewsContent2: [
            "api/news/news/html2/",
            "api/democracy/news/html2/"
        ],
        updateNews: [
            "api/news/update/",
            "api/democracy/update/"
        ],
        
        //PicGroup
        addPicGroup: [
            "api/news/saveCategoryImage/",
            "api/democracy/saveCategoryImage/"
        ],
        updatePicGroup: [
            "api/news/updateCategoryImage/",
            "api/democracy/updateCategoryImage/"
        ],
        
        //Topic
        addTopic: [
            "api/news/saveTopics/",
            "api/democracy/saveTopics/"
        ],
        updateTopic: [
            "api/news/updateTopics/",
            "api/democracy/updateTopics/"
        ],
        
        //Card
        addCard: [
            "api/news/saveCard",
            "api/democracy/saveCard"
        ],
        cancelCard: [
            "api/news/cancelCard/",
            "api/democracy/cancelCard/"
        ],
        updateCardSort: [
            "api/news/updateCardSort",
            "api/democracy/updateCardSort"
        ],
        
        //NewsList
        getNewsList: [
            "api/news/dateNews/",
            "api/democracy/dateNews/"
        ],
        searchNews: [
            "api/utils/search/moreCmsNews?",
            "api/utils/search/moreCmsDemocracy?"
        ],
        updateNewsStatus: [
            "api/news/updateNewsStatus/",
            "api/democracy/updateNewsStatus/"
        ],
        
        //CardList
        getCardList: [
            "api/news/sevenDates/",
            "api/democracy/sevenDates/"
        ],
        searchCard: [
            "api/news/cards/search?",
            "api/democracy/cards/search?"
        ],
        
        //Adv
        addAdvs: "api/news/pubAdv",
        getAdvs: "api/news/advs/",
        updateAdvs: "api/news/advs/update/",
        searchAdvs: "api/news/advs/search?",
        deletAdvs: "api/news/advs/delete/",
        
        //Comment
        searchComment: "api/news/comment/search?",
        updateComment: "api/news/comment/update",
        
        //Report
        getReportList: "api/news/otherList/warnings?",
        updateReport: "api/news/warning/update/",
        
        //FeedBack
        getFeedBackList: "api/news/otherList/feedbacks?",
        getFeedBackContent: "api/news/feedback/get/",
        updateFeedBack: "api/news/feedback/update/",
        
        //User
        getUserList: "api/u/list?",
        updateUserStatus: "api/u/updateStatus",
        resetUserNick: "api/u/reset",
        
        //admin
        getCaptcha: "captcha",  //formal
//        getCaptcha: "/auth/captcha", //develop
        addAdmin: "api/auth/addManger",
        deleteAdmin: "api/auth/deleteManger?",
        updateAdminStatus: "api/auth/grant?",
        getAdminList: "api/auth/manger",
        login: "api/auth/login",
        getEditor: "api/auth/getManger",
        
        //version
        uploadVersionApp: "fupload/version/file",
        updateVersionInfo: "fupload/fileinfo?",
        
        //authority
        getAuthList: "api/auth/getUApi",
        updateAuthStatus: "api/auth/updateUApi?",
        
        //statistics
        searchStatistics: "api/news/getAllStatistics?"
    },
    LANG = {
        msg: {
            error: [
                "成功~",
                "错误..",
                "您没有访问权限..",
                "请重新登录"
            ],
            login: [
                "登录成功~",
                "请完善表单..",
                "验证码错误..",
                "用户名或密码错误.."
            ],
            addNews: ["添加成功~", "添加失败.."],
            getNews: ["获取成功~", "获取失败.."],
            editNews: ["修改成功~", "修改失败.."]
        }
    };

function getArgs() {
    "use strict";
    var args = {},
        match = null,
        search = decodeURIComponent(location.search.substring(1)),
        reg = /(?:([^&]+)=([^&]+))/g;
    while ((match = reg.exec(search)) !== null) {
        args[match[1]] = match[2];
    }
    return args;
}

function setArgs() {
    "use strict";
    var args = getArgs(),
        title = [" - 京日关注", " - 生活资讯"];
    PAGE = 1;
    ID = args.id;
    ADVTYPE = args.ad;
    CHANNEL = args.c;
    TITLE = title[args.c];
    if (typeof (args.c) !== "undefined") {
        $("a").each(function () {
            $(this).attr("href", $(this).attr("href") + "?c=" + args.c);
        });
    }
    if (typeof (args.ad) !== "undefined") {
        $("a").each(function () {
            $(this).attr("href", $(this).attr("href") + "?ad=" + args.ad);
        });
    }
}

function getDate() {
    "use strict";
    var o, y, m, d;
    o = new Date();
    y = o.getFullYear();
    m = o.getMonth();
    d = o.getDate();
    if (m < 9) {
        m = "0" + (m + 1);
    } else {
        m = (m + 1);
    }
    if (d < 10) {
        d = "0" + d;
    }
    return y + "-" + m + "-" + d;
}

function getEditor() {
    "use strict";
    $.ajax({
        url: API.getEditor,
        processData: false,
        contentType: false,
        type: "GET",
        dataType: "json",
        success: function (data) {
            $("input[name=editor]").val(data.body.editor + "（" + data.body.editorcode + "）");
        }
    });
}

function initMain() {
    "use strict";
    if (typeof ($.cookie("USERLOGIN")) === "undefined") {
        $("#box-navigation").hide();
        $("iframe").attr("src", "login.html");
    } else {
        $("#box-navigation").show();
    }
    $("iframe").attr("src", $.cookie("POSITION"));
    $("#box-navigation").find("li[href='" + $.cookie("POSITION") + "']").addClass("active");
    $("iframe").on("load", function () {
        document.title = window.frames.container.document.title;
    });
    $("#navigation").on("click", "h1", function () {
        $("#box-navigation li").removeClass("active");
        $.cookie("POSITION", $(this).attr("href"));
        $("iframe").attr("src", $(this).attr("href"));
    });
    $("#box-navigation").on("click", "li", function () {
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
        $.cookie("POSITION", $(this).attr("href"));
        $("iframe").attr("src", $(this).attr("href"));
    });
}

function initLogin() {
    "use strict";
    $("#box-navigation", parent.document).hide();
    $("#box-login img").attr("src", API.getCaptcha);
    $("#box-login img").click(function () {
        location.reload();
    });
    $("#btn-login").click(function () {
        var form = new FormData($("#form-login")[0]);
        $.ajax({
            url: API.login,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                alert(LANG.msg.login[data.msg]);
                if (data.msg !== "0") {
                    location.reload();
/*
                    var src = API.getCaptcha + "?" + Math.random();
                    $("#box-login img").attr("src", src);
                    if (data.msg === "2") {
                        $("input [name=code]").val("");
                    }
                    if (data.msg === "3") {
                        $("input [name=username]").val("");
                        $("input [name=password]").val("");
                        $("input [name=code]").val("");
                    }
*/
                } else {
                    $.cookie("USERLOGIN", data.body.uid);
                    $.cookie("USERAUTH", data.body.auth.auth_id);
                    location.href = "home.html";
                    $("#box-navigation", parent.document).show();
                }
            }
        });
    });
    $("input").keypress(function (event) {
        if (event.which === 13) {
            if ($(this).attr("name") === "code") {
                $("#btn-login").focus().click();
            }
        }
    });
}

function initHome() {
    "use strict";
    $("#btn-quit").click(function () {
        $.removeCookie("USERLOGIN");
        $.removeCookie("POSITION");
        location.href = "login.html";
    });
}

function initArticle() {
    "use strict";
    if (typeof (ID) === "undefined") {
        document.title = "新建文章" + TITLE;
        getEditor();
        $("#summernote").summernote();
        $("#btn-submit").click(function () {
            var form;
            if ($("input[name='url']").val() !== "") {
                $("input[name='type']").val("2");
            }
            $("input[name='content']").val($("#summernote").code());
            form = $("#form-addnews").serialize();
            $.ajax({
                url: API.addNews[CHANNEL],
                data: form,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("添加失败..");
                    } else {
                        alert("添加成功~");
                        location.href = "list_news.html?c=" + CHANNEL;
                    }
                }
            });
        });
    } else {
        document.title = "编辑文章" + TITLE;
        var ajaxCount = 0;
        $.ajax({
            url: API.getNews[CHANNEL] + ID,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    console.log(data.body.nid);
                    $("input[name='title']").val(data.body.title);
                    $("input[name='source']").val(data.body.sources);
                    $("input[name='source_title']").val(data.body.source_title);
                    $("input[name='editor']").val(data.body.editor);
                    $("input[name='url']").val(data.body.urls);
                    $("input[name='keywords']").val(data.body.keywords);
                    $("input[name='keywords']").tagsinput("add", data.body.keywords);
                    $("input[name='listimage']").val(data.body.listimage);
                    $("input[name='largeimage']").val(data.body.largeimage);
                    $("input[name='smallimage']").val(data.body.smallimage);
                    $("textarea[name='description']").val(data.body.description);
                    $("#box-select").css("display", "block");
                    $("#box-select-a1").css("background-image", "url(" + API.viewImage + data.body.listimage + ")");
                    $("#box-select-b1").css("background-image", "url(" + API.viewImage + data.body.largeimage + ")");
                    $("#box-select-b2").css("background-image", "url(" + API.viewImage + data.body.smallimage + ")");
                    if (data.body.addtionalMap.adv.length > 0) {
                        $("input[name='adv']").val(data.body.addtionalMap.adv[0].aid);
                    }
                    $.each(data.body.addtionalMap.recNews, function (index, item) {
                        if (item.isSystem === false) {
                            $("input[name='recNews']").val(
                                $("input[name='recNews']").val() + item.nid + ","
                            );
                            $("input[name='recNews']").tagsinput("add", item.nid);
                        }
                    });
                }
            },
            complete: function () {
                ajaxCount += 1;
                if (ajaxCount === 2) {
                    $("#loading").hide();
                } else {
                    $("#loading").show();
                }
            }
        });
        $.ajax({
            url: API.getNewsContent2[CHANNEL] + ID,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "text",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data === "") {
                    $("#summernote").summernote();
                } else {
                    $("#summernote").html(data);
                    $("#summernote").summernote();
                }
            },
            complete: function () {
                ajaxCount += 1;
                if (ajaxCount === 2) {
                    $("#loading").hide();
                } else {
                    $("#loading").show();
                }
            }
        });
        $("#btn-submit").click(function () {
            var form;
            $(this).attr("class", "button off");
            if ($("input[name='url']").val() !== "") {
                $("input[name='type']").val("2");
            }
            $("input[name='content']").val($("#summernote").code());
            form = $("#form-addnews").serialize();
            $.ajax({
                url: API.updateNews[CHANNEL] + ID,
                data: form,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        console.log(data.body.nid);
                        alert("修改成功~");
                        location.href = "list_news.html?c=" + CHANNEL;
                    }
                }
            });
        });
    }
}

function initPicgroup() {
    "use strict";
    if (typeof (ID) === "undefined") {
        document.title = "新建组图" + TITLE;
        getEditor();
        $("#btn-submit").click(function () {
            var form;
            form = $("#form-addnews").serialize();
            $.ajax({
                url: API.addNews[CHANNEL],
                data: form,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("添加失败..");
                    } else {
                        alert("添加成功~请添加图片及描述~");
                        console.log(data.body.nid);
                        $("#form-addnews").hide();
                        $("#form-addpic").show();
                        $("#form-addpic").attr("belong", data.body.nid);
                        $("#btn-submit").hide();
                        $("#btn-confirm").css("display", "inline-block");
                    }
                }
            });
        });
        $("#picture").on("click", ".btn-savepic", function () {
            var here, url, description, form;
            here = $(this);
            url = here.parent().siblings(".box-pic").find("input").val();
            description = here.siblings("textarea").val();
            if (description === "") {
                alert("请填写描述信息！");
            } else {
                form = here.parents(".form-savepic").serialize();
                $.ajax({
                    url: API.saveImage,
                    data: form,
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        if (data.status === "3") {
                            alert(LANG.msg.error[3]);
                            location.href = "login.html";
                        }
                        console.log(data.body.pid);
                        here.parents(".picture").attr("pid", data.body.pid);
                        here.hide();
                        here.siblings().hide();
                        here.siblings("p").html(
                            here.siblings("textarea").val()
                        ).css("display", "inline-block");
                    }
                });
            }
        });
        $("#btn-confirm").click(function () {
            var form;
            $(".picture").each(function (index) {
                form = $(this).find("form").serialize();
                $.ajax({
                    url: API.addPicGroup[CHANNEL] + $("#form-addpic").attr("belong"),
                    data: form,
                    type: "POST",
                    async: false,
                    dataType: "json",
                    success: function (data) {
                        if (data.status === "3") {
                            alert(LANG.msg.error[3]);
                            location.href = "login.html";
                        }
                        if (data.status !== "0") {
                            alert("添加失败..");
                        } else {
                            location.href = "list_news.html?c=" + CHANNEL;
                        }
                    }
                });
            });
        });
    } else {
        document.title = "编辑组图" + TITLE;
        $.ajax({
            url: API.getNews[CHANNEL] + ID,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("获取失败..");
                } else {
                    console.log(data.body.nid);
                    $("input[name='title']").val(data.body.title);
                    $("input[name='source']").val(data.body.sources);
                    $("input[name='source_title']").val(data.body.source_title);
                    $("input[name='editor']").val(data.body.editor);
                    $("input[name='url']").val(data.body.urls);
                    $("input[name='keywords']").val(data.body.keywords);
                    $("input[name='keywords']").tagsinput("add", data.body.keywords);
                    $("input[name='listimage']").val(data.body.listimage);
                    $("input[name='largeimage']").val(data.body.largeimage);
                    $("input[name='smallimage']").val(data.body.smallimage);
                    $("textarea[name='description']").val(data.body.description);
                    $("#box-select").css("display", "block");
                    $("#box-select-a1").css("background-image", "url(" + API.viewImage + data.body.listimage + ")");
                    $("#box-select-b1").css("background-image", "url(" + API.viewImage + data.body.largeimage + ")");
                    $("#box-select-b2").css("background-image", "url(" + API.viewImage + data.body.smallimage + ")");
                    $.each(data.body.additionalList, function (index, item) {
                        $("#picture").append(
                            '<div class="picture">' +
                                '<form class="form-savepic">' +
                                '<div class="box-pic" style="background-image:url(' + API.viewImage + item.pid + ')">' +
                                '<div class="cover">X</div>' +
                                '<input type="text" name="url" value="' + item.url + '" />' +
                                '</div>' +
                                '<div class="box-dsc">' +
                                '<textarea rows="4" placeholder="添加描述，不超过200字" name="description">' + item.description + '</textarea>' +
                                '<div class="button btn-savepic">确认</div>' +
                                '<p></p>' +
                                '</div>' +
                                '</form>' +
                                '</div>'
                        );
                    });
                    if (data.body.addtionalMap.adv.length > 0) {
                        $("input[name='adv']").val(data.body.addtionalMap.adv[0].aid);
                    }
                    $.each(data.body.addtionalMap.recNews, function (index, item) {
                        if (item.isSystem === false) {
                            $("input[name='recNews']").val(
                                $("input[name='recNews']").val() + item.nid + ","
                            );
                            $("input[name='recNews']").tagsinput("add", item.nid);
                        }
                    });
                }
            }
        });
        $("#btn-submit").click(function () {
            var form;
            form = $("#form-addnews").serialize();
            $.ajax({
                url: API.updateNews[CHANNEL] + ID,
                data: form,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        alert("修改成功~请修改图片及描述~");
                        console.log(data.body.nid);
                        $("#form-addnews").hide();
                        $("#form-addpic").show();
                        $("#form-addpic").attr("belong", data.body.nid);
                        $("#btn-submit").hide();
                        $("#btn-confirm").css("display", "inline-block");
                    }
                }
            });
        });
        $("#picture").on("click", ".btn-savepic", function () {
            var here, url, description, form;
            here = $(this);
            url = here.parent().siblings(".box-pic").find("input").val();
            description = here.siblings("textarea").val();
            if (description === "") {
                alert("请填写描述信息！");
            } else {
                form = here.parents(".form-savepic").serialize();
                $.ajax({
                    url: API.saveImage,
                    data: form,
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        if (data.status === "3") {
                            alert(LANG.msg.error[3]);
                            location.href = "login.html";
                        }
                        console.log(data.body.pid);
                        here.parents(".picture").attr("pid", data.body.pid);
                        here.hide();
                        here.siblings().hide();
                        here.siblings("p").html(
                            here.siblings("textarea").val()
                        ).css("display", "inline-block");
                    }
                });
            }
        });
        $("#btn-confirm").click(function () {
            var form;
            form = new FormData();
            $(".picture").each(function () {
                form.append("pid", $(this).attr("pid"));
            });
            $.ajax({
                url: API.updatePicGroup[CHANNEL] + $("#form-addpic").attr("belong"),
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        location.href = "list_news.html?c=" + CHANNEL;
                    }
                }
            });
        });
    }
    $("#btn-image").click(function () {
        $("#btn-addpic").click();
    });
    $("#btn-addpic").click(function () {
        $(this).val("");
    });
    $("#btn-addpic").change(function () {
        var form, file = this;
        form = new FormData($("#form-addpic")[0]);
        $.ajax({
            url: API.uploadImage,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.addEventListener('progress', function (event) {
                    $("progress").attr("value", ((event.loaded / event.total) * 100));
                }, false);
                return xhr;
            },
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                var i, key;
                if (data.body.pic_1.status !== "0") {
                    alert(data.body.pic_1.msg);
                } else {
                    for (i = 0; i < file.files.length; i += 1) {
                        $("#picture").append(
                            '<div class="picture">' +
                                '<form class="form-savepic">' +
                                '<div class="box-pic" style="background-image:url(' + serv_url_assets + data.body["pic_" + (i + 1)].filePath + ')">' +
                                '<div class="cover">X</div>' +
                                '<input type="text" name="url" value="' + data.body["pic_" + (i + 1)].filePath + '"/>' +
                                '</div>' +
                                '<div class="box-dsc">' +
                                '<textarea rows="4" placeholder="添加描述，不超过200字" name="description"></textarea>' +
                                '<div class="button btn-savepic">确认</div>' +
                                '<p></p>' +
                                '</div>' +
                                '</form>' +
                                '</div>'
                        );
                    }
                }
            }
        });
    });
    $("#picture").on("click", ".cover", function () {
        $(this).parents(".picture").remove();
        if ($(".picture").length === 0) {
            $("progress").attr("value", 0);
        }
    });
}

function initTopic() {
    "use strict";
    if (typeof (ID) === "undefined") {
        document.title = "新建话题" + TITLE;
        $("#btn-submit").click(function () {
            var form;
            form = new FormData($("#form-addnews")[0]);
            $.ajax({
                url: API.addNews[CHANNEL],
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("添加失败..");
                    } else {
                        alert("添加成功~请添加子新闻~");
                        console.log(data.body.nid);
                        $("#form-addnews").hide();
                        $("#form-addtopic").show();
                        $("#form-addtopic").attr("belong", data.body.nid);
                        $("#btn-submit").hide();
                        $("#btn-confirm").css("display", "inline-block");
                    }
                }
            });
        });
        $("#btn-confirm").click(function () {
            var form;
            form = new FormData();
            $(".sortable input").each(function () {
                form.append("nid", $(this).val());
            });
            $.ajax({
                url: API.addTopic[CHANNEL] + $("#form-addtopic").attr("belong"),
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("添加失败..");
                    } else {
                        alert("添加成功~");
                        location.href = "list_topic.html?c=" + CHANNEL;
                    }
                }
            });
        });
    } else {
        document.title = "编辑话题" + TITLE;
        $.ajax({
            url: API.getNews[CHANNEL] + ID,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("获取失败..");
                } else {
                    console.log(data.body.nid);
                    $("#form-addtopic").attr("belong", data.body.nid);
                    $("input[name='title']").val(data.body.title);
                    $("input[name='keywords']").val(data.body.keywords);
                    $("input[name='keywords']").tagsinput("add", data.body.keywords);
                    $("input[name='listimage']").val(data.body.listimage);
                    $("input[name='largeimage']").val(data.body.largeimage);
                    $("input[name='smallimage']").val(data.body.smallimage);
                    $("textarea[name='description']").val(data.body.description);
                    $("#box-select").css("display", "block");
                    $("#box-select-a1").css("background-image", "url(" + API.viewImage + data.body.listimage + ")");
                    $("#box-select-b1").css("background-image", "url(" + API.viewImage + data.body.largeimage + ")");
                    $("#box-select-b2").css("background-image", "url(" + API.viewImage + data.body.smallimage + ")");
                    var newstype = ["标准新闻", "组图新闻", "语音新闻", "专题", "WEB新闻"];
                    $.each(data.body.additionalList, function (index, item) {
                        $(".sortable").append(
                            '<li class draggable="true">' +
                                '<div>' +
                                item.title +
                                '</div>' +
                                '<div>' +
                                item.pubdate +
                                '</div>' +
                                '<span>' +
                                newstype[item.type] +
                                '</span>' +
                                '<input type="hidden" value="' +
                                item.nid +
                                '" /></li>'
                        );
                    });
                    $(".sortable").sortable();
                }
            }
        });
        $("#btn-submit").click(function () {
            var form;
            form = new FormData($("#form-addnews")[0]);
            $.ajax({
                url: API.updateNews[CHANNEL] + ID,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        alert("修改成功~请修改子新闻~");
                        console.log(data.body.nid);
                        $("#form-addnews").hide();
                        $("#form-addtopic").show();
                        $("#form-addtopic").attr("belong", data.body.nid);
                        $("#btn-submit").hide();
                        $("#btn-confirm").css("display", "inline-block");
                    }
                }
            });
        });
        $("#btn-confirm").click(function () {
            var form;
            form = new FormData();
            $(".sortable input").each(function () {
                form.append("nid", $(this).val());
            });
            $.ajax({
                url: API.updateTopic[CHANNEL] + $("#form-addtopic").attr("belong"),
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        alert("修改成功~");
                        location.href = "list_topic.html?c=" + CHANNEL;
                    }
                }
            });
        });
    }
    $("#btn-getnews").click(function () {
        var nid = $("input[name=newsid]").val(),
            newstype = ["标准新闻", "组图新闻", "语音新闻", "专题", "WEB新闻"];
        $("input[name=newsid]").val("");
        $.ajax({
            url: API.getNews[CHANNEL] + nid,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("新闻不存在..");
                } else {
                    $(".sortable").append(
                        '<li class draggable="true">' +
                            '<div>' +
                            data.body.title +
                            '</div>' +
                            '<div>' +
                            data.body.createtime +
                            '</div>' +
                            '<span>' +
                            newstype[data.body.type] +
                            '</span>' +
                            '<input type="hidden" value="' +
                            nid +
                            '" /></li>'
                    );
                    $(".sortable").sortable();
                }
            }
        });
    });
    $(".sortable").on("click", "li", function () {
        $(this).remove();
    });
}

function initWeb() {
    "use strict";
    if (typeof (ID) === "undefined") {
        document.title = "新建WEB" + TITLE;
        getEditor();
        $("#btn-submit").click(function () {
            var form;
            form = new FormData($("#form-addnews")[0]);
            $.ajax({
                url: API.addNews[CHANNEL],
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("添加失败..");
                    } else {
                        console.log(data.body.nid);
                        alert("添加成功~");
                        location.href = "list_news.html?c=" + CHANNEL;

                    }
                }
            });
        });
    } else {
        document.title = "编辑WEB" + TITLE;
        $.ajax({
            url: API.getNews[CHANNEL] + ID,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("获取失败..");
                } else {
                    console.log(data.body.nid);
                    $("input[name='title']").val(data.body.title);
                    $("input[name='source']").val(data.body.sources);
                    $("input[name='source_title']").val(data.body.source_title);
                    $("input[name='editor']").val(data.body.editor);
                    $("input[name='url']").val(data.body.urls);
                    $("input[name='keywords']").val(data.body.keywords);
                    $("input[name='keywords']").tagsinput("add", data.body.keywords);
                    $("input[name='listimage']").val(data.body.listimage);
                    $("input[name='largeimage']").val(data.body.largeimage);
                    $("input[name='smallimage']").val(data.body.smallimage);
                    $("textarea[name='description']").val(data.body.description);
                    $("#box-select").css("display", "block");
                    $("#box-select-a1").css("background-image", "url(" + API.viewImage + data.body.listimage + ")");
                    $("#box-select-b1").css("background-image", "url(" + API.viewImage + data.body.largeimage + ")");
                    $("#box-select-b2").css("background-image", "url(" + API.viewImage + data.body.smallimage + ")");
                }
            }
        });
        $("#btn-submit").click(function () {
            var form;
            form = new FormData($("#form-addnews")[0]);
            $.ajax({
                url: API.updateNews[CHANNEL] + ID,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        console.log(data.body.nid);
                        alert("修改成功~");
                        location.href = "list_news.html?c=" + CHANNEL;
                    }
                }
            });
        });
    }
}

function initTemplate() {
    "use strict";
    function listnews(data) {
        $("#btn-today").attr("disabled", false);
        $.each(data, function (index, item) {
            switch (item.template) {
            case "0":
                $("#box-template").append(
                    '<div class="template" type="0" cid="' + item.cardID + '">' +
                        '<input type="hidden" name="sort" value="' + item.cardID + '"/>' +
                        '<div class="box-news">' +
                        '<div class="news">' +
                        '<div class="template-thumb-a" style="background-image:url(' + API.viewImage + item.news[0].listimage + ')"></div>' +
                        '<div class="template-title-a">' + item.news[0].title + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="box-control">' +
                        '<img class="btn-editnews" src="image/u169.png" alt="" />' +
                        '<img class="btn-copynews" src="image/u167.png" alt="" />' +
                        '<img class="btn-deletenews" src="image/u171.png" alt="" />' +
                        '</div>' +
                        '<div class="box-editnews">' +
                        '<form class="form-addnews">' +
                        '<div class="col">' +
                        '<p>ID</p>' +
                        '<input type="text" name="news_array" value="' + item.news[0].nid + '" />' +
                        '</div>' +
                        '<div class="col">' +
                        '<p>生效日期</p>' +
                        '<input type="date" name="pubdate" value="' + item.pubdate + '" />' +
                        '</div>' +
                        '<div class="col">' +
                        '<p></p>' +
                        '<div>' +
                        '<div class="button btn-submit">确定</div>' +
                        '<div class="button btn-cancel">取消</div>' +
                        '</div>' +
                        '</div>' +
                        '<input type="hidden" name="template" value="0">' +
                        '</form>' +
                        '</div>' +
                        '</div>'
                );
                break;
            case "1":
                $("#box-template").append(
                    '<div class="template" type="1" cid="' + item.cardID + '">' +
                        '<input type="hidden" name="sort" value="' + item.cardID + '"/>' +
                        '<div class="box-news">' +
                        '<div class="news">' +
                        '<div class="template-thumb-b1" style="background-image:url(' + API.viewImage + item.news[0].largeimage + ')"></div>' +
                        '<div class="template-title-b1">' + item.news[0].title + '</div>' +
                        '</div>' +
                        '<div class="news">' +
                        '<div class="template-thumb-b2" style="background-image:url(' + API.viewImage + item.news[1].smallimage + ')"></div>' +
                        '<div class="template-title-b2">' + item.news[1].title + '</div>' +
                        '</div>' +
                        '<div class="news">' +
                        '<div class="template-thumb-b3" style="background-image:url(' + API.viewImage + item.news[2].smallimage + ')"></div>' +
                        '<div class="template-title-b3">' + item.news[2].title + '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="box-control">' +
                        '<img class="btn-editnews" src="image/u169.png" alt="" />' +
                        '<img class="btn-copynews" src="image/u167.png" alt="" />' +
                        '<img class="btn-deletenews" src="image/u171.png" alt="" />' +
                        '</div>' +
                        '<div class="box-editnews">' +
                        '<form class="form-addnews">' +
                        '<div class="col">' +
                        '<p>ID1</p>' +
                        '<input type="text" name="news_array" value="' + item.news[0].nid + '" />' +
                        '</div>' +
                        '<div class="col">' +
                        '<p>ID2</p>' +
                        '<input type="text" name="news_array" value="' + item.news[1].nid + '" />' +
                        '</div>' +
                        '<div class="col">' +
                        '<p>ID3</p>' +
                        '<input type="text" name="news_array" value="' + item.news[2].nid + '" />' +
                        '</div>' +
                        '<div class="col">' +
                        '<p>生效日期</p>' +
                        '<input type="date" name="pubdate" value="' + item.pubdate + '" />' +
                        '</div>' +
                        '<div class="col">' +
                        '<p></p>' +
                        '<div>' +
                        '<div class="button btn-submit">确定</div>' +
                        '<div class="button btn-cancel">取消</div>' +
                        '</div>' +
                        '</div>' +
                        '<input type="hidden" name="template" value="1">' +
                        '</form>' +
                        '</div>' +
                        '</div>'
                );
                break;
            }
        });
        if ($.cookie("USERAUTH") === "3") {
            $(".box-control").hide();
            $("#box-submit").hide();
            $("#submit").hide();
        }
    }
    function datenews() {
        $("#btn-today").attr("disabled", true);
        $("#btn-keywords").val("");
        $("#box-template").html("");
        $.ajax({
            url: API.getCardList[CHANNEL] + $("#btn-today").val() + "?show=true",
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "1") {
                    alert("错误..");
                }
                if (data.status === "2") {
                    alert(LANG.msg.error[2]);
                }
                if (data.status === "0") {
                    if (data.body.indexList[0].data.length === 0) {
                        $("#btn-listmore").css("clear", "both").show();
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    }
                    listnews(data.body.indexList[0].data);
                    $("input[name='pubdate']").val(data.body.indexList[0].date);
                }
            }
        });
    }
    function searchnews() {
        $("#btn-today").attr("disabled", true);
        $.ajax({
            url: API.searchCard[CHANNEL] + "page=" + PAGE + "&nid=" + $("#btn-keywords").val(),
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("错误..");
                } else {
                    $("#btn-today").attr("disabled", false);
                    $.each(data.body.data, function (index, item) {
                        $("#box-template").append(
                            '<h1 style="clear:both">' + index + '</h1>'
                        );
                        listnews(item);
                    });
                    $("#btn-listmore").css("clear", "both").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                }
            }
        });
    }
    document.title += TITLE;
    $("#btn-today").val(getDate());
    datenews();
    $("#btn-today").on("change", datenews);
    $("#btn-search").on("click", function () {
        $("#box-template").html("");
        $("#btn-today").val("");
        PAGE = 1;
        searchnews();
    });
    $("#listnews").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#btn-sortnews").on("click", function () {
        $("#box-template").sortable();
        $(this).hide();
        $("#btn-savesort").css("display", "inline-block");
    });
    $("#btn-savesort").on("click", function () {
        var form;
        $("#box-template").sortable("destroy");
        $(this).hide();
        $("#btn-sortnews").css("display", "inline-block");
        form = new FormData();
        $("input[name='sort']").each(function () {
            form.append("sort", $(this).val());
        });
        $.ajax({
            url: API.updateCardSort[CHANNEL],
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("错误..");
                } else {
                    alert("排序成功~");
                }
            }
        });

    });
    $(".btn-addtemplate").on("click", function () {
        var tempcol;
        tempcol = $(this).data("col");
        switch (tempcol) {
        case "a":
            $("#box-template").append(
                '<div class="template" type="0" cid="0">' +
                    '<div class="box-news">' +
                    '<div class="news">' +
                    '<div class="template-thumb-a">缩略图</div>' +
                    '<div class="template-title-a">文章标题</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="box-control">' +
                    '<img class="btn-editnews" src="image/u169.png" alt="" />' +
                    '<img class="btn-copynews" src="image/u167.png" alt="" />' +
                    '<img class="btn-deletenews" src="image/u171.png" alt="" />' +
                    '</div>' +
                    '<div class="box-editnews" style="display:block">' +
                    '<form class="form-addnews">' +
                    '<div class="col">' +
                    '<p>ID</p>' +
                    '<input type="text" name="news_array" />' +
                    '</div>' +
                    '<div class="col">' +
                    '<p>生效日期</p>' +
                    '<input type="date" name="pubdate" />' +
                    '</div>' +
                    '<div class="col">' +
                    '<p></p>' +
                    '<div>' +
                    '<div class="button btn-submit">确定</div>' +
                    '<div class="button btn-cancel">取消</div>' +
                    '</div>' +
                    '</div>' +
                    '<input type="hidden" name="template" value="0">' +
                    '</form>' +
                    '</div>' +
                    '</div>'
            );
            break;
        case "b":
            $("#box-template").append(
                '<div class="template" type="1" cid="0">' +
                    '<div class="box-news">' +
                    '<div class="news">' +
                    '<div class="template-thumb-b1">缩略图</div>' +
                    '<div class="template-title-b1">文章标题</div>' +
                    '</div>' +
                    '<div class="news">' +
                    '<div class="template-thumb-b2">缩略图</div>' +
                    '<div class="template-title-b2">文章标题</div>' +
                    '</div>' +
                    '<div class="news">' +
                    '<div class="template-thumb-b3">缩略图</div>' +
                    '<div class="template-title-b3">文章标题</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="box-control">' +
                    '<img class="btn-editnews" src="image/u169.png" alt="" />' +
                    '<img class="btn-copynews" src="image/u167.png" alt="" />' +
                    '<img class="btn-deletenews" src="image/u171.png" alt="" />' +
                    '</div>' +
                    '<div class="box-editnews" style="display:block">' +
                    '<form class="form-addnews">' +
                    '<div class="col">' +
                    '<p>ID1</p>' +
                    '<input type="text" name="news_array" />' +
                    '</div>' +
                    '<div class="col">' +
                    '<p>ID2</p>' +
                    '<input type="text" name="news_array" />' +
                    '</div>' +
                    '<div class="col">' +
                    '<p>ID3</p>' +
                    '<input type="text" name="news_array" />' +
                    '</div>' +
                    '<div class="col">' +
                    '<p>生效日期</p>' +
                    '<input type="date" name="pubdate" />' +
                    '</div>' +
                    '<div class="col">' +
                    '<p></p>' +
                    '<div>' +
                    '<div class="button btn-submit">确定</div>' +
                    '<div class="button btn-cancel">取消</div>' +
                    '</div>' +
                    '</div>' +
                    '<input type="hidden" name="template" value="1">' +
                    '</form>' +
                    '</div>' +
                    '</div>'
            );
            break;
        }
    });
    $("#box-template").on("click", ".btn-copynews", function () {
        var newtemp;
        newtemp = $(this).parents(".template");
        newtemp.parent().append(
            '<div class="template" type="' + newtemp.attr("type") + '" cid="0">' + newtemp.html() + '</div>'
        );
        newtemp.parent().children("[cid='0']").find(".btn-editnews").click();
    });
    $("#box-template").on("click", ".btn-editnews", function () {
        $(this).parent().siblings(".box-editnews").show();
    });
    $("#box-template").on("click", ".btn-deletenews", function () {
        var oldtemp;
        oldtemp = $(this).parents(".template");
        if (oldtemp.attr("cid") === "0") {
            oldtemp.remove();
        } else if (confirm("确认?")) {
            $.ajax({
                url: API.cancelCard[CHANNEL] + oldtemp.attr("cid"),
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("Wrong ID!");
                    } else {
                        oldtemp.remove();
                    }
                }
            });
        }
    });
    $("#box-template").on("click", ".btn-cancel", function () {
        var temp;
        temp = $(this).parents(".template");
        if (temp.attr("cid") === "0") {
            temp.remove();
        } else {
            $(this).parents(".box-editnews").hide();
        }
    });
    $("#box-template").on("click", ".btn-submit", function () {
        var root, box, news, flag, field, form;
        root = $(this).parents(".box-editnews");
        box = root.siblings(".box-news");
        news = root.find("input[name='news_array']");
        if (root.parent().attr("cid") !== "0") {
            root.siblings(".box-control").children(".btn-deletenews").click();
        }
        news.each(function (index, item) {
            $.ajax({
                url: API.searchNews[CHANNEL] + $(this).val(),
                processData: false,
                contentType: false,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("请填写正确ID");
                    } else {
                        if (news.length === 1) {
                            flag = "a";
                            field = "listimage";
                        }
                        if (news.length === 3) {
                            switch (index) {
                            case 0:
                                flag = "b1";
                                field = "largeimage";
                                break;
                            case 1:
                                flag = "b2";
                                field = "smallimage";
                                break;
                            case 2:
                                flag = "b3";
                                field = "smallimage";
                                break;
                            }
                        }
                        box.find(".template-thumb-" + flag).css("background-image", "url(" + API.viewImage + data.body[field] + ")").html("");
                        box.find(".template-title-" + flag).html(data.body.title);
                    }
                }
            });
        });
        form = new FormData(root.children(".form-addnews")[0]);
        $.ajax({
            url: API.addCard[CHANNEL],
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    alert("发布成功~");
                    location.reload();
                }
            }
        });
    });
}

function initAdvertise() {
    "use strict";
    if (typeof (ID) === "undefined") {
        document.title = "新建广告";
        $("#btn-submit").click(function () {
            var form;
            form = new FormData($("#form-addnews")[0]);
            document.title = "新建广告";
            $.ajax({
                url: API.addAdvs,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("添加失败..");
                    } else {
                        console.log(data.body.id);
                        alert("添加成功~");
                        location.href = "list_advertise.html?ad=" + ADVTYPE;
                    }
                }
            });
        });
    } else {
        document.title = "编辑广告";
        $.ajax({
            url: API.getAdvs + ID,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("获取失败..");
                } else {
                    console.log(data.body.id);
                    $("input[type='radio'][name='target'][value='" + data.body.target + "']").click();
                    $("input[type='radio'][name='type'][value='" + data.body.type + "']").click();
                    $("input[name='title']").val(data.body.title);
                    $("input[name='largeimage']").val(data.body.largeimage);
                    $("input[name='content']").val(data.body.content);
                    $("input[name='beginTime']").val(data.body.beginTime.substring(0, 10));
                    $("input[name='endTime']").val(data.body.endTime.substring(0, 10));
                    $("#box-select").css("display", "block");
                    $("#box-select-b1").css("background-image", "url(" + API.viewImage + data.body.largeimage + ")");
                    $("#box-select-a1").css("background-image", "url(" + API.viewImage + data.body.largeimage + ")");
                }
            }
        });
        $("#btn-submit").click(function () {
            var form;
            form = new FormData($("#form-addnews")[0]);
            $.ajax({
                url: API.updateAdvs + ID,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("修改失败..");
                    } else {
                        alert("修改成功~");
                        location.href = "list_advertise.html?ad=" + ADVTYPE;
                    }
                }
            });
        });
    }
    if (ADVTYPE === "0") {
        $("input[name='target']").val("0");
        $("input[name='target']").after("文章广告");
        $("#box-select div").width("249px");
        $("#box-select div").height("40px");
    } else {
        $("input[name='target']").val("1");
        $("input[name='target']").after("启动广告");
        $("#box-select div").attr("id", "box-select-a1");
        $("#box-select div").width("360px");
        $("#box-select div").height("640px");
    }
    $("input[name='type']").click(function () {
        if ($(this).val() === "2") {
            $("input[name='content']").attr("type", "hidden").val("null");
        } else {
            $("input[name='content']").attr("type", "text").val("");
        }
    });
}

function initNewsList() {
    "use strict";
    function listnews(data) {
        $("#btn-today").attr("disabled", false);
        $.each(data, function (index, item) {
            var newstype, operate;
            newstype = ["标准新闻", "组图新闻", "语音新闻", "", "WEB新闻"];
            if (item.type !== "3") {
                switch (item.type) {
                case "0":
                    operate = '<span class="btn-operate" data-o="3" data-t="0">查看</span> | <span class="btn-operate" data-o="1" data-t="0">编辑</span> | <span class="btn-operate" data-o="2">冻结</span>';
                    break;
                case "1":
                    operate = '<span class="btn-operate" data-o="3" data-t="1">查看</span> | <span class="btn-operate" data-o="1" data-t="1">编辑</span> | <span class="btn-operate" data-o="2">冻结</span>';
                    break;
                case "2":
                    operate = '<span class="btn-operate" data-o="3" data-t="0">查看</span> | <span class="btn-operate" data-o="1" data-t="2">编辑</span> | <span class="btn-operate" data-o="2">冻结</span>';
                    break;
                case "4":
                    operate = '<span class="btn-operate" data-o="3" data-t="4" data-u="' + item.urls + '">查看</span> | <span class="btn-operate" data-o="1" data-t="4">编辑</span> | <span class="btn-operate" data-o="2">冻结</span>';
                    break;
                }
                if (item.status === "1") {
                    operate = '已冻结';
                }
                $('tbody').append(
                    '<tr nid=' + item.nid + '>' +
                        '<td>' + item.nid + '</td>' +
                        '<td>' + item.createtime + '</td>' +
                        '<td>' + item.title + '</td>' +
                        '<td>' + item.editor + '</td>' +
                        '<td>' + newstype[item.type] + '</td>' +
                        '<td>' + item.statistics + '</td>' +
                        '<td>' + operate + '</td>' +
                        '</tr>'
                );
            }
        });
        if ($.cookie("USERAUTH") === "3") {
            $(".btn-operate[data-o=1]").hide();
            $(".btn-operate[data-o=2]").hide();
            $("#box-submit").hide();
        }
    }
    function datenews() {
        $("#btn-today").attr("disabled", true);
        $("#btn-keywords").val("");
        $("tbody").html("");
        $.ajax({
            url: API.getNewsList[CHANNEL] + $("#btn-today").val(),
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    listnews(data.body.indexList);
                    $("#btn-listmore").hide();
                }
            }
        });
    }
    function searchnews() {
        $("#btn-today").val("");
        $("#btn-today").attr("disabled", true);
        $.ajax({
            url: API.searchNews[CHANNEL] + "keywords=" + $("#btn-keywords").val() + "&page=" + PAGE,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    listnews(data.body.data);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                }
            }
        });
    }
    document.title += TITLE;
    $("#btn-today").val(getDate());
    datenews();
    $("#btn-today").on("change", datenews);
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#box-table").on("click", ".btn-operate", function () {
        var nid, form, href;
        nid = $(this).parents("tr").attr("nid");
        switch ($(this).data("o")) {
        case 1:
            switch ($(this).data("t")) {
            case 0:
                location.href = "add_article.html?c=" + CHANNEL + "&id=" + nid;
                break;
            case 1:
                location.href = "add_picgroup.html?c=" + CHANNEL + "&id=" + nid;
                break;
            case 2:
                location.href = "add_article.html?c=" + CHANNEL + "&id=" + nid;
                break;
            case 4:
                location.href = "add_web.html?c=" + CHANNEL + "&id=" + nid;
                break;
            }
            break;
        case 2:
            form = new FormData();
            form.append("nid", nid);
            form.append("uid", "test");
            form.append("status", "1");
            $.ajax({
                url: API.updateNewsStatus[CHANNEL] + nid,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                        location.reload();
                    }
                }
            });
            break;
        case 3:
            switch ($(this).data("t")) {
            case 0:
                href = "detail_article.html?c=" + CHANNEL + "&id=" + nid;
                break;
            case 1:
                href = "detail_picgroup.html?c=" + CHANNEL + "&id=" + nid;
                break;
            case 2:
                href = "detail_article.html?c=" + CHANNEL + "&id=" + nid;
                break;
            case 4:
                href = $(this).data("u");
                break;
            }
            window.open(href, 'newwindow', 'height=850,width=800,top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no');
            break;
        }
    });
}

function initTopicList() {
    "use strict";
    function listnews(data) {
        $("#btn-today").attr("disabled", false);
        $.each(data, function (index, item) {
            var operate;
            if (item.type === "3") {
                operate = '<span class="btn-operate" data-o="1">编辑</span> | <span class="btn-operate" data-o="2">冻结</span>';
                if (item.status === "1") {
                    operate = '已冻结';
                }
                $('tbody').append(
                    '<tr nid=' + item.nid + '>' +
                        '<td>' + item.nid + '</td>' +
                        '<td>' + item.createtime.substr(0, 16) + '</td>' +
                        '<td>' + item.title + '</td>' +
//                        '<td></td>' +
                        '<td>' + operate + '</td>' +
                        '</tr>'
                );
            }
        });
        if ($.cookie("USERAUTH") === "3") {
            $(".btn-operate").hide();
            $("#box-submit").hide();
        }
    }
    function datenews() {
        $("#btn-today").attr("disabled", true);
        $("#btn-keywords").val("");
        $("tbody").html("");
        $.ajax({
            url: API.getNewsList[CHANNEL] + $("#btn-today").val(),
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    listnews(data.body.indexList);
                }
            }
        });
    }
    function searchnews() {
        $("#btn-today").val("");
        $("#btn-today").attr("disabled", true);
        $.ajax({
            url: API.searchNews[CHANNEL] + "keywords=" + $("#btn-keywords").val() + "&page=" + PAGE,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    listnews(data.body.data);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                }
            }
        });
    }
    document.title += TITLE;
    $("#btn-today").val(getDate());
    datenews();
    $("#btn-today").on("change", datenews);
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#box-table").on("click", ".btn-operate", function () {
        var nid, form;
        nid = $(this).parents("tr").attr("nid");
        switch ($(this).data("o")) {
        case 1:
            location.href = "add_topic.html?c=" + CHANNEL + "&id=" + nid;
            break;
        case 2:
            form = new FormData();
            form.append("nid", nid);
            form.append("status", "1");
            form.append("uid", "test");
            $.ajax({
                url: API.updateNewsStatus[CHANNEL] + nid,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                        location.reload();
                    }
                }
            });
            break;
        }
    });
}

function initAdvertiseList() {
    "use strict";
    function searchnews() {
        $("#btn-filter").attr("disabled", true);
        var url = API.searchAdvs + "page=" + PAGE + "&target=" + ADVTYPE;
        if ($("#btn-keywords").val() !== "") {
            url += "&title=" + $("#btn-keywords").val();
        }
        if ($("#btn-filter").val() !== "") {
            url += "&status=" + $("#btn-filter").val();
        }
        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    $("#btn-filter").attr("disabled", false);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                    $.each(data.body.data, function (index, item) {
                        var status = ["生效中", "未开始", "已过期"];
                        $("tbody").append(
                            '<tr aid="' + item.aid + '" >' +
                                '<td>' + item.aid + '</td>' +
                                '<td>' + item.createtime + '</td>' +
                                '<td>' + item.title + '</td>' +
                                '<td style="background-image:url(' + API.viewImage + item.largeimage + ')"></td>' +
                                '<td>' + item.begintime.substring(0, 10) + ' ' + item.endtime.substring(0, 10) + '</td>' +
                                '<td>' + item.content + '</td>' +
                                '<td>' + status[item.adv_status] + '</td>' +
//                                '<td><span class="btn-operate" data-o="3">查看</span></td>' +
                                '<td><span class="btn-operate" data-o="1">编辑</span> | <span class="btn-operate" data-o="2">删除</span></td>' +
                                '</tr>'
                        );
                    });
                }
            }
        });
    }
    searchnews();
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#box-table").on("click", ".btn-operate", function () {
        var aid;
        aid = $(this).parents("tr").attr("aid");
        switch ($(this).data("o")) {
        case 1:
            location.href = "add_advertise.html?ad=" + ADVTYPE + "&id=" + aid;
            break;
        case 2:
            if (confirm("确认删除?")) {
                $.ajax({
                    url: API.deletAdvs + aid,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    dataType: "json",
                    success: function (data) {
                        if (data.status === "3") {
                            alert(LANG.msg.error[3]);
                            location.href = "login.html";
                        }
                        if (data.status !== "0") {
                            alert("错误..");
                        } else {
                            alert("成功~");
                            location.reload();
                        }
                    }
                });
            }
            break;
        }
    });
}

function initUserComment() {
    "use strict";
    function searchnews() {
        var url = API.searchComment + "page=" + PAGE;
        $("#btn-filter").attr("disabled", true);
        if ($("#btn-keywords").val() !== "") {
            url += "&keyword=" + $("#btn-keywords").val();
        }
        if ($("#btn-filter").val() !== "") {
            url += "&status=" + $("#btn-filter").val();
        }
        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    $("#btn-filter").attr("disabled", false);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                    $.each(data.body.data, function (index, item) {
                        var status, operate, check;
                        status = ["正常", "未审核", "冻结"];
                        operate = [
                            '<span class="btn-operate" data-o="2">冻结</span>',
                            '<span class="btn-operate" data-o="1">通过审核</span> | <span class="btn-operate" data-o="2">冻结</span>',
                            '<span class="btn-operate" data-o="3">解冻</span>'
                        ];
                        check = [
                            '',
                            '<input type="checkbox" />',
                            ''
                        ];
                        $("tbody").append(
                            '<tr cid="' + item.cid + '" >' +
                                '<td>' + check[item.status] + '</td>' +
                                '<td>' + item.comment_time + '</td>' +
                                '<td>' + item.user.userName + '</td>' +
                                '<td>' + item.comment + '</td>' +
                                '<td>' + status[item.status] + '</td>' +
                                '<td></td>' +
                                '<td>' + operate[item.status] + '</td>' +
                                '</tr>'
                        );
                    });
                }
            }
        });
    }
    searchnews();
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#listcomment").on("click", ".btn-operate", function () {
        var cid, status, form;
        form = new FormData();
        if ($(this).data("o") === 4) {
            $("tbody input[type='checkbox']:checked").each(function () {
                cid = $(this).parents("tr").attr("cid");
                form.append("cid", cid);
            });
            status = "0";
        } else {
            cid = $(this).parents("tr").attr("cid");
            switch ($(this).data("o")) {
            case 1:
                status = "0";
                break;
            case 2:
                status = "2";
                break;
            case 3:
                status = "0";
                break;
            }
            form.append("cid", cid);
        }
        form.append("status", status);
        $.ajax({
            url: API.updateComment,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("错误..");
                } else {
                    alert("成功~");
                    location.reload();
                }
            }
        });
    });
    $("#box-table").on("click", "thead input[type='checkbox']", function () {
        if ($(this).prop("checked")) {
            $("tbody input[type='checkbox']").prop("checked", true);
        } else {
            $("tbody input[type='checkbox']").prop("checked", false);
        }
    });
    $("#box-table").on("click", "tbody input[type='checkbox']", function () {
        if ($("tbody input[type='checkbox']:checked").length === $("tbody input[type='checkbox']").length) {
            $("thead input[type='checkbox']").prop("checked", true);
        } else {
            $("thead input[type='checkbox']").prop("checked", false);
        }
    });
}

function initUserReport() {
    "use strict";
    function searchnews() {
        var url = API.getReportList + "page=" + PAGE;
        $("#btn-filter").attr("disabled", true);
        if ($("#btn-filter").val() !== "") {
            url += "&status=" + $("#btn-filter").val();
        }
        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    $("#btn-filter").attr("disabled", false);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                    $.each(data.body.data, function (index, item) {
                        var status, reporttype, operate;
                        status = ["撤销", "未处理", "冻结"];
                        reporttype = ["侮辱诋毁", "广告推销", "谣言", "色情", "其他"];
                        operate = [
                            '',
                            '<span class="btn-operate" data-o="1">撤销</span> | <span class="btn-operate" data-o="2">冻结</span>',
                            '<span class="btn-operate" data-o="3">解冻</span>'
                        ];
                        $("tbody").append(
                            '<tr wid="' + item.wid + '" >' +
                                '<td>' + item.comment_time + '</td>' +
                                '<td>' + item.cuser.userName + '</td>' +
                                '<td>' + item.ccomment.comment + '</td>' +
                                '<td>' + item.user.userName + '</td>' +
                                '<td>' + reporttype[item.type] + '</td>' +
                                '<td>' + status[item.status] + '</td>' +
                                '<td></td>' +
                                '<td>' + operate[item.status] + '</td>' +
                                '</tr>'
                        );
                    });
                }
            }
        });
    }
    searchnews();
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#box-table").on("click", ".btn-operate", function () {
        var wid, status, form;
        wid = $(this).parents("tr").attr("wid");
        switch ($(this).data("o")) {
        case 1:
            status = "0";
            break;
        case 2:
            status = "2";
            break;
        case 3:
            status = "0";
            break;
        }
        form = new FormData();
        form.append("status", status);
        $.ajax({
            url: API.updateReport + wid,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("错误..");
                } else {
                    alert("成功~");
                    location.reload();
                }
            }
        });
    });
}

function initUserFeedback() {
    "use strict";
    function searchnews() {
        var url = API.getFeedBackList + "page=" + PAGE;
        $("#btn-filter").attr("disabled", true);
        if ($("#btn-filter").val() !== "") {
            url += "&status=" + $("#btn-filter").val();
        }
        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "2") {
                    alert(LANG.msg.error[2]);
                }
                if (data.status === "0") {
                    $("#btn-filter").attr("disabled", false);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                    $.each(data.body.data, function (index, item) {
                        var status, operate;
                        status = ["已处理", "未处理", "不予处理"];
                        $("tbody").append(
                            '<tr wid="' + item.wid + '" >' +
                                '<td>' + item.createTime + '</td>' +
                                '<td>' + item.user.userid + '</td>' +
                                '<td>' + item.user.userName + '</td>' +
                                '<td>' + status[item.status] + '</td>' +
                                '<td></td>' +
                                '<td><span class="btn-operate" data-o="3">查看</span></td>' +
                                '</tr>'
                        );
                    });
                }
            }
        });
    }
    searchnews();
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#listcomment").on("click", ".btn-operate", function () {
        var wid, status, operate, form;
        if ($(this).data("o") === 3) {
            $("#box-comment").show();
            wid = $(this).parents("tr").attr("wid");
            $.ajax({
                url: API.getFeedBackContent + wid,
                processData: false,
                contentType: false,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        $("#comment").html('<p>' + data.body.data[0].wid + '</p>');
                        $.each(data.body.data, function (index, item) {
                            if (item.type === "0") {
                                $("#comment").append(
                                    '<div class="comment">' +
                                        '<p>' + item.createTime + '</p>' +
                                        '<p>' + item.comment + '</p>' +
                                        '</div>'
                                );
                            } else {
                                $("#comment").append(
                                    '<div class="comment">' +
                                        '<p>' + item.createTime + '</p>' +
                                        '<p><img src="' + serv_url_assets + item.comment + '"></p>' +
                                        '</div>'
                                );
                            }
                        });
                        operate = [
                            '<div class="button" id="btn-close">关闭</div>',
                            '<div class="button btn-operate" data-o="1">处理</div><div class="button btn-operate" data-o="2">不予处理</div><div class="button" id="btn-close">关闭</div>',
                            '<div class="button" id="btn-close">关闭</div>'
                        ];
                        $("#operate").html(operate[data.body.data[0].status]);
                    }
                }
            });
        } else {
            switch ($(this).data("o")) {
            case 1:
                status = "0";
                break;
            case 2:
                status = "2";
                break;
            }
            form = new FormData();
            form.append("status", status);
            wid = $(this).parent().siblings("#comment").children("p").html();
            $.ajax({
                url: API.updateFeedBack + wid,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                        location.reload();
                    }
                }
            });
        }
    });
    $("#listcomment").on("click", "#btn-close", function () {
        $("#box-comment").hide();
    });
}

function initUserList() {
    "use strict";
    function searchnews() {
        var url = API.getUserList + "amount=20&current=" + PAGE;
        $("#btn-filter").attr("disabled", true);
        if ($("#btn-keywords").val() !== "") {
            url += "&nickname=" + $("#btn-keywords").val();
        }
        if ($("#btn-filter").val() !== "") {
            url += "&status=" + $("#btn-filter").val();
        }
        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "2") {
                    alert(LANG.msg.error[2]);
                }
                if (data.status === "0") {
                    $("#btn-filter").attr("disabled", false);
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                    $.each(data.body.data, function (index, item) {
                        var status, operate;
                        status = ["正常", "冻结"];
                        operate = [
                            '<span class="btn-operate" data-o="1">冻结</span> | <span class="btn-operate" data-o="3">重置昵称</span>',
                            '<span class="btn-operate" data-o="2">解冻</span>'
                        ];
                        $("tbody").append(
                            '<tr uid="' + item.id + '" >' +
                                '<td>' + item.id + '</td>' +
                                '<td>' + item.createtime + '</td>' +
                                '<td>' + item.nickname + '</td>' +
                                '<td>' + item.mobile + '</td>' +
                                '<td>' + item.platform + '</td>' +
                                '<td>' + status[item.status] + '</td>' +
                                '<td>' + operate[item.status] + '</td>' +
                                '</tr>'
                        );
                    });
                }
            }
        });
    }
    searchnews();
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        PAGE = 1;
        searchnews();
    });
    $("#box-table").on("click", ".hasmore", function () {
        PAGE += 1;
        searchnews();
    });
    $("#box-table").on("click", ".btn-operate", function () {
        var uid, status, form;
        uid = $(this).parents("tr").attr("uid");
        if ($(this).data("o") === 3) {
            form = new FormData();
            form.append("id", uid);
            $.ajax({
                url: API.resetUserNick,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                    }
                }
            });
        } else {
            switch ($(this).data("o")) {
            case 1:
                status = "1";
                break;
            case 2:
                status = "0";
                break;
            }
            form = new FormData();
            form.append("id", uid);
            form.append("status", status);
            $.ajax({
                url: API.updateUserStatus,
                data: form,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                        location.reload();
                    }
                }
            });
        }
    });
}

function initAdminList() {
    "use strict";
    function searchnews() {
        $.ajax({
            url: API.getAdminList,
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "2") {
                    alert(LANG.msg.error[2]);
                }
                if (data.status === "0") {
                    $("#btn-listmore").show();
                    if (data.body.hasNext === false) {
                        $("#btn-listmore").html("没有更多..").removeClass("hasmore").off("click");
                    } else {
                        $("#btn-listmore").html("查看更多..").addClass("hasmore");
                    }
                    $.each(data.body.data, function (index, item) {
                        var status, operate;
                        status = ["", "SA", "高级编辑", "普通编辑"];
                        operate = [
                            "",
                            "",
                            '<span class="btn-operate" data-o="1">冻结</span> | <span class="btn-operate" data-o="2">降低权限</span>',
                            '<span class="btn-operate" data-o="1">冻结</span> | <span class="btn-operate" data-o="3">升高权限</span>'
                        ];
                        operate = operate[item.authority.auth_id];
                        if (item.manager.status === "1") {
                            operate = "已冻结";
                        }
                        $("tbody").append(
                            '<tr mid="' + item.manager.id + '" >' +
                                '<td>' + item.manager.id + '</td>' +
                                '<td>' + item.manager.username + '</td>' +
                                '<td>' + item.manager.password + '</td>' +
                                '<td>' + item.manager.editor + '</td>' +
                                '<td>' + item.manager.editorcode + '</td>' +
                                '<td>' + status[item.authority.auth_id] + '</td>' +
                                '<td>' + operate + '</td>' +
                                '</tr>'
                        );
                    });
                }
            }
        });
    }
    searchnews();
    $("#btn-addadmin").click(function () {
        $("#box-editor").show();
    });
    $("#btn-close").click(function () {
        $("#box-editor").hide();
    });
    $("#btn-submit").click(function () {
        var form = new FormData($("#form-addeditor")[0]);
        $.ajax({
            url: API.addAdmin,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("添加失败..");
                } else {
                    alert("添加成功~");
                    location.reload();
                }
            }
        });
    });
    $("#box-table").on("click", ".btn-operate", function () {
        var mid, status, form;
        mid = $(this).parents("tr").attr("mid");
        if ($(this).data("o") === 1) {
            $.ajax({
                url: API.deleteAdmin + "mid=" + mid,
                data: form,
                processData: false,
                contentType: false,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                        location.reload();
                    }
                }
            });
        } else {
            switch ($(this).data("o")) {
            case 2:
                status = "3";
                break;
            case 3:
                status = "2";
                break;
            }
            $.ajax({
                url: API.updateAdminStatus + "uid=" + mid + "&authids=" + status,
                data: form,
                processData: false,
                contentType: false,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        alert("成功~");
                        location.reload();
                    }
                }
            });
        }
    });
}

function initArticleDetail() {
    "use strict";
    document.title = "查看文章" + TITLE;
}

function initPicgroupDetail() {
    "use strict";
    document.title = "查看组图" + TITLE;
}

function initVersion() {
    "use strict";
    $("[name=version]").attr("disabled", true);
    $("[name=limitVersion]").attr("disabled", true);
    $("[name=content]").attr("disabled", true);
    $("#btn-submit2").hide();
    $("#btn-submit1").on("click", function () {
        var form = new FormData($("#form-upload")[0]);
        $("#btn-submit1").html("上传中");
        $.ajax({
            url: API.uploadVersionApp,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.body.app_1.status !== "0") {
                    alert("上传失败..");
                } else {
                    alert("上传成功~");
                    $("#btn-submit1").html("上传");
                    $("input[name=app]").val("");
                    $("input[name=url]").val(data.body.app_1.filePath);
                    $("input[name=version]").attr("disabled", false);
                    $("input[name=limitVersion]").attr("disabled", false);
                    $("textarea[name=content]").attr("disabled", false);
                    $("#btn-submit2").show();
                }
            }
        });
    });
    $("#btn-submit2").on("click", function () {
        var url = "version=" + $("input[name=version]").val() + "&limitVersion=" + $("input[name=limitVersion]").val() + "&content=" + $("textarea[name=content]").val() + "&url=" + $("input[name=url]").val();
        $.ajax({
            url: API.updateVersionInfo + url,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status !== "0") {
                    alert("错误..");
                } else {
                    alert("成功~");
                    location.reload();
                }
            }
        });
    });
}

function initAuthority() {
    "use strict";
    var i,
        b = [{s: "", a: "", h: "", d: [1, 5]},
             {s: "", a: "", h: "", d: [2, 6]}],
        active = ["已启用", "已禁用"],
        hover = ["禁用", "启用"];
    function onclick(x) {
        $.each(b[x].d, function (index, item) {
            var url = "status=" + b[x].s + "&id=" + b[x].d[index];
            $.ajax({
                url: API.updateAuthStatus + url,
                processData: false,
                contentType: false,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        location.reload();
                    }
                }
            });
        });
    }
    $.ajax({
        url: API.getAuthList,
        processData: false,
        contentType: false,
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data.status === "3") {
                alert(LANG.msg.error[3]);
                location.href = "login.html";
            }
            if (data.status !== "0") {
                alert("错误..");
            } else {
                for (i = 0; i < 2; i += 1) {
                    if (data.body[i].status === "1") {
                        b[i].s = 0;
                        b[i].a = active[1];
                        b[i].h = hover[1];
                    } else {
                        b[i].s = 1;
                        b[i].a = active[0];
                        b[i].h = hover[0];
                    }
                }
                $(".button").each(function (index, item) {
                    $(item).html(b[index].a);
                    $(item).hover(function () {
                        $(item).html(b[index].h);
                    }, function () {
                        $(item).html(b[index].a);
                    });
                    $(item).click(function () {
                        onclick(index);
                    });
                });
            }
        }
    });
}

function initStatistics() {
    "use strict";
    function listnews(data) {
        $("#btn-begin").attr("disabled", false);
        $("#btn-end").attr("disabled", false);
        $.each(data, function (index, item) {
            $('tbody').append(
                '<tr>' +
                    '<td>' + item.editor + '</td>' +
                    '<td>' + item.total + '</td>' +
                    '<td>' + item.count + '</td>' +
                    '<td>' + item.average + '</td>' +
                    '</tr>'
            );
        });
    }
    function searchnews() {
        $("#btn-begin").attr("disabled", true);
        $("#btn-end").attr("disabled", true);
        $.ajax({
            url: API.searchStatistics + "beginTime=" + $("#btn-begin").val() + "&endTime=" + $("#btn-end").val() + "&keywords=" + $("#btn-keywords").val(),
            processData: false,
            contentType: false,
            type: "GET",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.status === "0") {
                    listnews(data.body.list);
                }
            }
        });
    }
    document.title += TITLE;
    $("#btn-begin").val(getDate());
    $("#btn-end").val(getDate());
    searchnews();
    $("#btn-search").on("click", function () {
        $("tbody").html("");
        searchnews();
    });
}

function initTest() {
    "use strict";
}

$(function () {
    "use strict";
    setArgs();
    $.ajaxSetup({
        beforeSend: function () {
            $("#loading").show();
        },
        complete: function () {
            $("#loading").hide();
        }
    });
    $("#box-thumb img").each(function (index, item) {
        var set;
        set = [[247, 223], [247, 112], [100, 80]];
        if (document.title === "新建广告" || document.title === "编辑广告") {
            if (ADVTYPE === "0") {
                set = [[249, 40]];
            } else {
                set = [[360, 640]];
            }
        }
        $(this).cropper({
            aspectRatio: set[index][0] / set[index][1],
            strict: true,
            guides: false,
            autoCropArea: 1,
            checkImageOrigin: false,
            highlight: false,
            dragCrop: false,
            cropBoxMovable: false,
            cropBoxResizable: false,
            built: function () {
                $(this).cropper("setCropBoxData", {
                    width: set[index][0],
                    height: set[index][1],
                    top: (330 - set[index][1]) / 2,
                    left: (370 - set[index][0]) / 2
                });
            }
        });
    });
    $("#btn-uppic").click(function () {
        $("input[name=thumb]").click().val("");
    });
    $("input[name=thumb]").change(function () {
        var form;
        form = new FormData($("#form-uppic")[0]);
        $.ajax({
            url: API.uploadImage,
            data: form,
            processData: false,
            contentType: false,
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status === "3") {
                    alert(LANG.msg.error[3]);
                    location.href = "login.html";
                }
                if (data.body.thumb_1.status !== "0") {
                    alert("错误..");
                } else {
                    $("#box-thumb").attr("src", data.body.thumb_1.filePath).css("display", "inline-block");
                    $("#box-thumb img").cropper("replace", serv_url_assets + data.body.thumb_1.filePath);
                    $("#btn-savethumb").show();
                }
            }
        });
    });
    $("body").on("click", "#btn-savethumb", function () {
        $("#box-thumb input").each(function (index, item) {
            var form1, form2, thumb, type, cropData;
            thumb = [
                "#box-thumb-a1",
                "#box-thumb-b1",
                "#box-thumb-b2"
            ];
            type = index;
            if (document.title === "新建广告" || document.title === "编辑广告") {
                thumb = ["#box-thumb-b1"];
                if (ADVTYPE === "0") {
                    type = 3;
                } else {
                    type = 4;
                }
            }
            cropData = $(thumb[index]).cropper("getData");
            form1 = new FormData();
            form1.append("src", $("#box-thumb").attr("src"));
            form1.append("w", Math.round(cropData.width));
            form1.append("h", Math.round(cropData.height));
            form1.append("x", Math.round(cropData.x));
            form1.append("y", Math.round(cropData.y));
            form1.append("type", type);
            $.ajax({
                url: API.uploadScale,
                data: form1,
                async: false,
                processData: false,
                contentType: false,
                type: "POST",
                dataType: "json",
                success: function (data) {
                    if (data.status === "3") {
                        alert(LANG.msg.error[3]);
                        location.href = "login.html";
                    }
                    if (data.status !== "0") {
                        alert("错误..");
                    } else {
                        form2 = new FormData();
                        form2.append("url", data.body);
                        form2.append("description", "thumb");
                        $.ajax({
                            url: API.saveImage,
                            data: form2,
                            async: false,
                            processData: false,
                            contentType: false,
                            type: "POST",
                            dataType: "json",
                            success: function (data) {
                                console.log(data.body.pid);
                                $(item).val(data.body.pid);
                                $("#box-select div").eq(index).css("background-image", "url(" + API.viewImage + data.body.pid + ")");
                                $("#box-thumb").hide();
                                $("#box-select").css("display", "block");
                                $("#btn-savethumb").hide();
                                
                            }
                        });
                    }
                }
            });
        });
    });
    $("#btn-keywords").keypress(function (event) {
        if (event.which === 13) {
            $("#btn-search").click();
        }
    });
    $("body").on("keypress", "textarea", function (event) {
        if (event.which === 13) {
            $(this).blur();
        }
    });
    $("body").on("keydown", "textarea", function (event) {
        if (event.which === 13) {
            $(this).blur();
        }
    });
    $("body").on("change", "textarea", function (event) {
        $(this).val($(this).val().replace(/\"/g, ""));
        $(this).val($(this).val().replace(/\[/g, ""));
        $(this).val($(this).val().replace(/\]/g, ""));
        $(this).val($(this).val().replace(/\{/g, ""));
        $(this).val($(this).val().replace(/\}/g, ""));
        $(this).val($(this).val().replace(/\n/g, ""));
        $(this).val($(this).val().replace(/\r/g, ""));
        $(this).val($(this).val().replace(/\t/g, ""));
        $(this).val($(this).val().replace(/\\/g, "/"));
    });
    $("input[type!='file']").change(function (event) {
        $(this).val($(this).val().replace(/\"/g, ""));
        $(this).val($(this).val().replace(/\[/g, ""));
        $(this).val($(this).val().replace(/\]/g, ""));
        $(this).val($(this).val().replace(/\{/g, ""));
        $(this).val($(this).val().replace(/\}/g, ""));
        $(this).val($(this).val().replace(/\\/g, "/"));
    });
});
